# frozen_string_literal: true

require "link_helper"

module Liquid
  class StageInArtifact < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, filename, tokens)
      super
      @filename = filename.strip
    end

    def render(context)
      computation = context.registers[:computation]
      url = computation_artifacts_url(computation, secret: computation.secret,
        name: @filename, script_name: script_name(computation))
      "curl -L -o $SCRATCHDIR/#{@filename} #{url}"
    end
  end
end
