# frozen_string_literal: true

class Liquid::LicenseFor < Liquid::Tag
  def initialize(tag_name, type, tokens)
    super
    @type = type.strip
  end

  def render(context)
    license = context.registers[:organization].license_for(@type)
    if license
      license.to_script
    else
      context.registers[:errors]
        .add(:script, "cannot find requested #{@type} license")
    end
  end
end
