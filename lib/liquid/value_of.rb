# frozen_string_literal: true

class Liquid::ValueOf < Liquid::Tag
  def initialize(_tag_name, key, _tokens)
    super
    @key = key.strip
  end

  def render(context)
    pv = context.registers[:computation].parameter_value_for(@key)

    if pv
      pv.value
    else
      context.registers[:errors]
        .add(:script, "parameter value #{@key} was not found")
    end
  end
end
