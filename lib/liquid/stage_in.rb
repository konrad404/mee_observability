# frozen_string_literal: true

require "link_helper"

module Liquid
  class StageIn < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, parameters, tokens)
      super
      parameters = parameters.split
      @data_file_type_string = parameters.shift
      @filename = parameters.join(" ") unless parameters.empty?
    end

    def render(context)
      computation = context.registers[:computation]
      filename = @filename || computation.pick_file(@data_file_type_string)&.filename
      if filename && @data_file_type_string
        url = computation_inputs_url(computation, secret: computation.secret,
          type: @data_file_type_string, script_name: script_name(computation))
        "curl -L -o \"$SCRATCHDIR/#{filename}\" #{url}"
      else
        context.registers[:errors]
          .add(:script, "cannot find #{@data_file_type_string} data file in patient or pipeline directories")
      end
    end
  end
end
