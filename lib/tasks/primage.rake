# frozen_string_literal: true

namespace :primage do
  desc "Setup primage organization"
  task setup: :environment do
    DemoOrganization.create! name: "PRIMAGE", logo_path: "db/logos/primage.png",
      ssh_key_path: ENV["PIPELINE_SSH_KEY"],
      gitlab_api_private_token: ENV["GITLAB_API_PRIVATE_TOKEN"],
      plgrid_team_id: "plggprimage",
      grants: {
        "plgprimage4-cpu": Date.parse("2022-07-04"),
        "plgprimage4-gpu": Date.parse("2022-07-04"),
        "plgprimage4-gpu-a100": Date.parse("2022-07-04"),
        "plgprimage5-cpu": Date.parse("2023-07-03"),
        "plgprimage5-gpu": Date.parse("2023-07-03"),
        "plgprimage5-gpu-a100": Date.parse("2023-07-03"),
      }
  end
end
