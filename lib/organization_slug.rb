# frozen_string_literal: true

module OrganizationSlug
  PATTERN = /(\d{7,})/
  FORMAT = "%07d"

  def self.decode(slug) = slug.to_i
  def self.encode(id) = FORMAT % id

  # We're using organization id prefixes in the URL path. Rather than namespace
  # all our routes, we're "mounting" the Rails app at this URL prefix.
  #
  # The Extractor middleware yanks the prefix off PATH_INFO, moves it to
  # SCRIPT_NAME, and drops the account id in env['mee.organization_id'].
  #
  # Rails routes on PATH_INFO and builds URLs that respect SCRIPT_NAME,
  # so the main app is none the wiser. We look up the current organization using
  # env['mee.organization_id'] instead of request.subdomains.first
  class Extractor
    PATH_INFO_MATCH = /\A(\/#{OrganizationSlug::PATTERN})/

    def initialize(app)
      @app = app
    end

    def call(env)
      request = ActionDispatch::Request.new(env)

      # $1, $2, $' == script_name, slug, path_info
      if request.path_info =~ PATH_INFO_MATCH
        request.script_name   = $1
        request.path_info     = $'.empty? ? "/" : $'

        # Stash the organization id
        env["mee.organization_id"] = OrganizationSlug.decode($2)
      end

      @app.call env
    end
  end

  # Limit session cookies to the slug path.
  class LimitSessionToAccountSlugPath
    def initialize(app)
      @app = app
    end

    def call(env)
      env["rack.session.options"][:path] = env["SCRIPT_NAME"] if env["mee.organization_id"]
      @app.call env
    end
  end
end
