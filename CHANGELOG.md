
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

Please view this file on the master branch, on stable branches it's out of date.

## Unreleased

### Added
  - Add new primage grant do `primage:setup` rake task (@mkasztelnik)
  - Add message when stage_out fails (@piotrpolec)

### Changed
  - Dependencies upgrade (@mkasztelnik)
  - Use default active storage and mailer queues (@mkasztelnik)
  - Merge `RimrockStep` with `Step` (@mkasztelnik)
  - Merge `RimrockComputation` with `Computation` (@mkasztelnik)
  - Update step manual (@piotrpolec)
  - Simplify organization repository validation logic (@mkasztelnik)

### Deprecated

### Removed
  - Remove organization specific data storage (@mkasztelnik)
  - Remove user JWT token (@mkasztelnik)
  - Remove unused `notifications` sidekiq queue (@mkasztelnik)
  - Remove not used routes constraints (@mkasztelnik)
  - Remove one time run rake tasks (@mkasztelnik)
  - Remove `tag_or_version` from computation (@mkasztelnik)
  - Get rid of step create and update services (@mkasztelnik)
  - Get rid of supervisor role (@piotrpolec)

### Fixed
 - Redirect after deleting organization (@piotrpolec)
 - Unhanndled error when step repo is wrong (@piotrpolec)


## 4.0.1 (12.07.2023)

### Changed
  - Moved duplicate output removal to delayed job (@piotrpolec)

### Fixed
  - Fixed uploading many outputs in parallel (@piotrpolec)

## 4.0.0 (29.06.2023)

### Added
  - Grant type presence is validated by model and DB (@piotrpolec)
  - Add cohorts and campaigns (@youngdashu, @ChrisGadek)
  - Added i18n-task gem, translations fitted healthy (@piotrwawryka)
  - Make pipeline status persistent (@piotrpolec)
  - Repository validation after organization update (@sanotaras)
  - Add button to refresh files manually (@piotrpolec)
  - New liquid tags to route file manipulation through MEE (@piotrpolec)
  - Add possibility to upload artifacts from local files (@piotrpolec)
  - Organization admins mail notification grant expire soon (@piotrwawryka)
  - Add storage usage per organization and limit functionalities based on it (@piotrpolec)

### Changed
  - Run periodic validation jobs only in production (@mkasztelnik)
  - Pipeline status calculation extracted to separate class (@piotrpolec)
  - Campaign status calculation extracted  to separate class (@piotrpolec)
  - `PersistentError` model for storing steps, pipelines errors instead of custom ones (@ChrisGadek, @mkasztelnik)
  - Migrate rspec tests to minitest
  - Migrate from subdomain to root domain with app mounted in organization scope (@mkasztelnik)
  - Change cohort view to include tabs (@ChrisGadek)
  - On medium mobile devices in tables only selected columns are displayed (@piotrwawryka)
  - Update prometheus group directory in demo organization (@mkasztelnik)

### Deprecated

### Removed
  - Remove unused rspec json expectation gem (@mkasztelnik)
  - Remove `counter_culture` gem in favor of custom counter cache update (@mkasztelnik)

### Fixed
  - Fix S3 copy and move methods (@mkasztelnik)

### Security


## 3.1.0 (27.12.2022)

### Added
  - Added artifacts - organization wide files to use in pipelines (@piotrpolec)
  - Custom made `Proxy#==` method (@mkasztelnik)
  - Add possibility to override system user while generating demo organization (@mkasztelnik)
  - Ability to change site the computations are run on (@piotrpolec)
  - Logging tag for every organization in production environment (@ChrisGadek)
  - Unconfigured automatic pipelines can be configured before running (@piotrpolec)
  - Add S3 storage (@mkasztelnik, @ChrisGadek)

### Changed
  - Switch from `byebug` to `debug` (@mkasztelnik)
  - Switch from `pry` to `irb` (@mkasztelnik)
  - Dependencies update (@mkasztelnik)
  - Set session cookie expire time to 12 hours (@mkasztelnik)
  - Make PLGrid team id mandatory (@mkasztelnik)
  - Query Rimrock about concrete jobs instead of tag (@mkasztelnik)
  - Changed pipeline parameters loading to turbo (@youngdashu)
  - Move fetching logs functions to computation and site (@piotrpolec)
  - Storage configuration stored as jsonb inside organization and serialized to own class (@mkasztelnik)
  - Use site to fetch stdout and stderr (@mkasztelnik)

### Deprecated

### Removed
  - Remove EGI Checkin login (@mkasztelnik)
  - Remove REST API (@mkasztelnik)
  - Remove Devise and use pure omniauth (@mkasztelnik)
  - Remove Webdav computations (@mkasztelnik)
  - Remove roles from Organization (@mkasztelnik)
  - Remove EurValve specific configuration and EurValve specific conditions (@mksztelnik)

### Fixed
  - Fix steps and flows view when there are none defined (@piotrpolec)
  - Fixed translation in computation section (@piotrwawryka)

### Security


## 3.0.0 (22.06.2022)

### Added
  - Shows warning to the user if the user is trying to run computation with
    discarded flow (@ChrisGadek)
  - Shows little exclamation mark near pipeline status in the patients view if
    pipeline has discarded flow(@ChrisGadek)
  - If a newly created PLGrid account is in an organization group, it is automatically
    accepted, no need for admin approval (@piotrpolec)
  - User management based on PLGrid user teams (@piotrpolec)
  - Add `SENTRY_ENVIRONMENT` to override default sentry environment (@mkasztelnik)
  - Add pipelines filtering and pipeline output files filtering (@ChrisGadek)
  - User can delete input/output files of the pipeline (@piotrpolec)
  - Ability to create organization pipeline (@piotrpolec)

### Changed
  - Directory creation for new pipeline is now asynchronous (@piotrpolec)
  - Update link to GitLab token webpage (@mkasztelnik)
  - Dependencies upgrade (@mkasztelnik)
  - use `jsbundling` (with `esbuild`) and `cssbundling` instead of `webpacker` (@mkasztelnik)
  - Simplified URL for pipelines (@piotrpolec)
  - use `turbo` instead of `turbolinks` (@mkasztelnik)
  - Upgrade to rails 7 (@mkasztelnik)
  - Upgrade to ruby 3.1.1 (@mkasztelnik)

### Deprecated

### Removed
  - Remove support for the FileStore as patient/pipeline files storage (@mkasztelnik)
  - Remove off viewer connected with the FileStore (@mkasztelnik)
  - Remove duplicated `protect_from_forgery` from `ApplicationController`,
    use default from `ActionController::Base` (@mkasztelnik)
  - Removed 'pipeline#ordered' function (@piotrpolec)
  - Remove pdp (groups, services, resources) (@mkasztelnik)

### Fixed
  - Error on registration via OpenID with a missing argument in method
    `set_flash_message!` (@ChrisGadek)
  - Fixed error with logging for the first time without organization
    created (@ChrisGadek)
  - Pipeline view slow loading (@piotrpolec)
  - Don't show `-` at the end of computation status (@mkasztelnik)

### Security

## 2.6.1 (17.06.2022)

### Fixed

- Fix loading patient from correct organization (@mkasztelnik)
- Fix data file synchronization uses only organization data file types (@mkasztelnik)

## 2.6.0 (04.11.2021)

### Added
- Ability to change Git client for the organization and individual step (@damiansosnowski)
- Native Git client (@damiansosnowski)
- Docker image with development and production stages (@damiansosnowski)
- Docker Compose configuration for development and production deployments (@damiansosnowski)
- Pipelines for patients now have notes (@piotrpolec)
- Pagination of elements (@piotrpolec)
- Edit and delete buttons for pipelines in patients view (@piotrpolec)
- Tests: computation correctly updates its' status when in pipeline view (@piotrpolec)
- Soft-delete for flows (@piotrpolec)
- Enable normal users to create steps and flows (@piotrpolec)

### Changed
- Redesign patient index view (@mkasztelnik)
- In Organization and Steps Git config is now stored in git_config field (@damiansosnowski)
- Development DB username and password are used only when defined as ENV
  variables (@mkasztelnik)
- Update to ruby 3.0.2
- Updated Chrome installation snippet (apt-key deprecated)
- Wrapped proxy string with Proxy model (@piotrpolec)

### Deprecated

### Removed
- Remove `pry-doc` and `pry-byebug` (@mkasztelnik)

### Fixed
- Number parameter default value should be optional
- Grants and licenses that start in the future are now properly labeled (@piotrpolec)
- Fix pipeline status colors on patient view with possibility to compare pipelines (@mkasztelnik)
- Fix pipeline edit form (@mkasztelnik)
- Error message after pipeline rerun is cleared (@piotrpolec)

### Security

## 2.5.1

### Changed
- Disabled registration (@damiansosnowski)

## 2.5.0

### Added
- User proxy certificate can be injected to the slurm starting script (@mkasztelnik)
- Show Gitlab download key fingerprint and use it to validate key (@mkasztelnik)
- RequiredDataFile parameter to allow choosing between input files (@damiansosnowski)
- Add possibility to customize Prometheus HPC host via ENV variable (@mkasztelnik)

### Changed
- Gitlab download key is provided as file not text input (@mkasztelnik)
- Dependencies upgraded (@mkasztelnik)
- SSHKey gem replaced with ssh_data gem to support newer SSH keys (@damiansosnowski)
- Made Rimrock error status message in computations more prominent to the user (@damiansosnowski)
- Download pipeline file by clicking on the file name (@mkasztelnik)
- Update stimulusjs to 2.0.0 (@mkasztelnik)
- Update JS dependencies (@mkasztelnik)
- Increase number of returned branches and tags by gitlab api to 100 (@mkasztelnmik)
- Upgade to ruby 2.7.2 (@mkasztelnik)
- Upgrade rails to 6.1, upgrade dependencies (@mkasztelnik)
- Gitlab token is shown using showable password input (@mkasztelnik)
- Select default grant in grant parameter value when there is only one active grant (@mkasztelnik)
- Select default branch in model version parameter value (@mkasztelnik)
- Change step name to be unique in the organization scope (@mkasztelnik)
- `clone_repo` liquid helper takes repository path from computation (@mkasztelnik)

### Deprecated

### Removed
- Key validation from TokenValidator (@damiansosnowski)
- Remove unused pipeline form view (@mkasztelnik)

### Fixed
- Patient and pipeline directories are create with group write permissions (@mkasztelnik)
- Admin step action does not load steps from other organization (@mkasztelnik)
- Ssh key validation (@damiansosnowski)
- Remove duplicated `ROLES` constant definition (@mkasztelnik)
- clone_repo tag did not use step's custom ssh key and git host was such was defined (@damiansosnowski)
- choices.js includes valid/invalid border color (@mkasztelnik)
- Show correct step configuration view (automatic or manual) after pipeline
  create validation error (@mkasztelnik)

### Security
- Protect omniauth request phase against CSRF (@mkasztelnik)


## 2.4.0

### Added
- Add new PRIMAGE grant to `primage:setup` rake task (@mkasztelnik)
- Restore text diffing capability in comparison view (@mkasztelnik)
- Storing RimrockComputation logs in the database (@damiansosnowski)
- Ability to abort a running Computation (@damiansosnowski)
- Add step parameters and computation parameter values (@mkasztelnik)
- User can select which grant to use when running computations (@damiansosnowski)
- Gitlab configuration can be step specific (@mkasztelnik)
- Add possibility to edit user membership roles (@mkasztelnik)

### Changed
- Terms of use introduction updated (@mkasztelnik)
- Patient `case_number` is now used as name, added slug with FriendlyId in it's place (@damiansosnowski)
- Remove not needed organization eager loader while creating new pipeline (@mkasztelnik)
- License form uses `dynamic-form` controller to manage license entries (@mkasztelnik)
- Grant type is mandatory for RimrockStep (@damiansosnowski)

### Deprecated

### Removed

### Fixed
- PatientChannel and ComputationChannel are authorized in context of current user and organization (@damiansosnowski)

### Security

## 2.3.1

### Fixed
- Load organization patient in pipeline controller (@mkasztelnik)

## 2.3.0

### Added
- Token validation for specific repositories (@damiansosnowski)
- stage_out check for existence of the file (@damiansosnowski)
- Registration terms of use (@mkasztelnik)

### Changed
- Use turbolink reload meta tag instead of turbolinks: false in links (@mkasztelnik)

### Deprecated

### Removed

### Fixed
- Error is shown when trying to create an organization with restricted friendlyId slug (@damiansosnowski)

### Security


## 2.2.1

### Fixed
- GWT FileStore browser requires `window.jQuery` (@mkasztelnik)
- GWT FileStore errors is shown using `alert` function (@mkasztelnik)


## 2.2.0

### Added
- Discard delayed job on deserialization error (@mkasztelnik)
- Checkin IDP integration (@mkasztelnik)
- Checkin membership auto-accept (@mkasztelnik)

### Changed
- Step slug is organization scoped (@mkasztelnik)
- Data file type organization changed to mandatory (@mkasztelnik)
- Updated dependencies and rails to 6.0.3.1 (@mkasztelnik)
- Updated webpack to 5.1.1 (@mkasztelnik)
- Delayed job UI moved to root domain (@mkasztelnik)
- Redesigned organization navbar and side menu (@mkasztelnik)
- Script generation errors are shown to the user instead of pasting error to the
  generated slurm starting script (@mkasztelnik)
- stage_in only supports stage in datafile type (@damiansosnowski)
- Omniauth uses Rails logger (@mkasztelnik)
- Redirect to organization show after update (@mkasztelnik)
- Rename `./bin/dev-server` to `./bin/dev` (@mkasztelnik)

### Deprecated

### Removed
- Remove `mee:dft_organization` rake task (@mkasztelnik)
- Remove data sets views and data sets patient statistics (@mkasztelnik)
- Remove deprecated `setup_ansys_licenses` script generator helper (@mkasztelnik)

### Fixed
- Remove real request to external services during normal tests run (@mkasztelnik)
- Make `eurvalve:setup` task steps slugs static (@mkasztelnik)
- Print logs to stdout from web while running `/bin/dev-server` (@mkasztelnik)
- Fix showing multiple file store GWT widget (@mkasztelnik)

### Security


## 2.1.0

### Added
- Webpacker added to manage modern JS (@mkasztelnik)
- Stimulus JS added for simple UI components (@mkasztelnik)
- NodeJS version in asdf-vm .tool_config file (@damiansosnowski)
- Logout link on organization membership "not a member", "not approved" and "blocked" pages (@damiansosnowski)
- User session is shared between organizations (@mkasztelnik)
- Error is displayed when there is no script to run on a branch/tag (@damiansosnowski)
- User can register/sign up using root webpage (@mkasztelnik)
- Organization creation form (@damiansosnowski)
- Data file type is organization scoped (@mkasztelnik)
- Organization edit form (@damiansosnowski)
- Use overmind instead of foreman in present in the classpath (@mkasztelnik)
- Data file types CRUD (@mkasztelnik)
- Organization deletion (@damiansosnowski)
- Grants CRUD (@damiansosnowski)
- Pipeline flows CRUD (@mkasztelnik)
- Licenses CRUD (@damiansosnowski, @mkasztelnik)
- Splash screens for patients, flows, steps, data file types, grants and licenses (@mkasztelnik)
- Scheduled organization validation checks with errors persisting until fixed (@damiansosnowski)
- Public ssh key validation check (@damiansosnowski)
- Asynchronous step repository ssh key validation check - after save and periodic (@damiansosnowski)

### Changed
- Dedicated rake task (`eurvalve:setup`) created to setup eurvalve organization (@mkasztelnik)
- Dependencies upgrade (@mkasztelnik)
- Use ruby 2.6.5 (@mkasztelnik)
- Use stimulus controller to manage pipeline websocket subscription (@mkasztelnik)
- Use stimulus controller to manage patient websocket subscription (@mkasztelnik)
- Updated README.md to include asdf-vm and Yarn installation. Some improvements to the text (@damiansosnowski)
- Reenabled Yarn dependency downloading in bin/setup file (@damiansosnowski)
- Pipeline comparison button is only enabled when two pipelines are selected (@damiansosnowski)
- Changed default hostname in production environment (@damiansosnowski)
- Changed StartRunnable call invocations to StartRunnableJob perform later (@damiansosnowski)
- Use code format similar used in rails (rubocop-rails_config gem) (@mkasztelnik)
- Move from features tests to system tests (@mkasztelnik)

### Deprecated

### Removed
- Rake tasks to create steps and flows removed (use organization specific setup
  task instead) (@mkasztelnik)
- Remove data file type enum from data file (@mkasztelnik)
- Remove outdated prime:dev tasks, use `primage:setup`, `eurvalve:setup` instead (@mkasztelnik)
- Remove step names from flow (@mkasztelnik)

### Fixed
- Return 404 (instead of 500) when pipeline or pipeline computation is not found (@mkasztelnik)
- Added PostgreSQL password to fix CI failing due to blank password in the database (@damiansosnowski)
- Changed `order` to `position` to fix sample data import (@damiansosnowski)
- Fixed lack of exit_code checking for command failures when using RimRock (@damiansosnowski)
- Fixed too short resource names in tests that would sometimes appear in the metadata token (@damiansosnowski)
- "Generate proxy" warning bar now also appears on manual steps that have finished computing (@damiansosnowski)
- Fixed wrong configuration in dev environment that resulted in app trying to send mails (@damiansosnowski)
- Fix Gitlab host is taken from organization not from static configuration (@mkasztelnik)
- Fix connecting stimulus computation controller multiple times (@mkasztelnik)
- Change DOC line brakes to Linux line breaks (@mkasztelnik)
- Data file synchronizer is organization scoped (@mkasztelnik)
- Fix translations for admin users section (@mkasztelnik)
- `stage_in` liquid tag is organization scoped (@mkasztelnik)

### Security

## 2.0.0

### Added
- Many organization can be defined in MEE (@mkasztelnik)
- Patient, group, service and flow belongs to organization (@mkasztelnik)
- Add organization capabilities (@mkasztelnik)
- Custom organization domain (@mkasztelnik)
- Storage specific for the organization (@mkasztelnik)
- Organization specific logo (@mkasztelnik)
- Grant type (e.g. cpu, gpu) (@mkasztelnik)
- Licenses are organization scoped (@mkasztelnik)
- New `license_for` liquid tag (@mkasztelnik)

### Changed
- Use organization name as portal title (@mkasztelnik)
- Dependencies updated (@mkasztelnik)
- Organization name on devise views (@mkasztelnik)
- Store flow step in DB (@mkasztelnik)
- Store organization specific grant in DB (@mkasztelnik)
- Organization specific Gitlab configuration (@mkasztelnik)
- Small improvoement for home index page layout (@mkasztelnik)
- Updated manuals and UI components to reflect MEE's multiorganizational nature (@p.nowakowski)
- Patient case number uniqueness is organization scoped (@mkasztelnik)

### Deprecated

### Removed
- Remove all patient pipelines outputs browser (@mkasztelnik)

### Fixed
- Fix `rack_attack` notification subscribe configuration (@mkasztelnik)

### Security

## 1.0.0

### Added
- Full Flow DB model to replace FLOWS constant for dynamic flow definitions (@Nuanda)

### Changed
- Refactored localisation code for computation steps messages (@Nuanda)
- Dependencies upgraded (@mkasztelnik)
- Rename Vapor to MEE (@mkasztelnik)

### Deprecated

### Removed
- Remove cloud resources (@mkasztelnik)
- Remove info about guard from readme (@mkasztelnik)

### Fixed
- Ensure that computation is started only once (@mkasztelnik)

### Security

## 0.14.0

### Added
- Insert ANSYS license server exports only when licenses are defined (@mkasztelnik)

### Changed
- Rename "AV Segmenation" to "Segmentation" (@mkasztelnik)

### Deprecated

### Removed

### Fixed
- Fix problem with device form login (@mkasztelnik)

### Security

## 0.13.0

### Added
- All active user computation are aborted when user is deleted (@mkasztelnik)
- Pipelines API (@Nuanda)
- New provenance data file type (@mkasztelnik)
- Data file synchronization API (@mkasztelnik)
- Description how to start https development server using puma (@mkasztelnik)
- Data file type stored in DB (@mkasztelnik)
- Start runnable computation after new file appears (@mkasztelnik)

### Changed
- Move application controller configurations to separate concerns (@mkasztelnik)

### Deprecated

### Removed
- Remove `thin` from `Gemfile` since puma can be used to start https development server (@mkasztelnik)
- Remove `Patient#procedure_status` not used anymore (@mkasztelnik)
- Remove `guard` since no one from the team is using it right now (@mkasztelnik)
- Remove dummy "Design new pipeline" button (@Nuanda)

### Fixed
- Issue #361 regression when running a rimrock step without tag or branch (@Nuanda)
- Don't duplicate admin group membership while running seeds multiple time (@mkasztelnik)

### Security

## 0.12.2

### Security

- Rails upgraded with fixes for: CVE-2019-5418, CVE-2019-5419 and CVE-2019-5420 (@mkasztelnik)

## 0.12.1

### Fixed
- Fix pipeline list: CRC model CFD/ROM pipelines (@mkasztelnik)

## 0.12.0

### Added
- Pipelines API (@Nuanda)
- New provenance data file type (@mkasztelnik)
- Add FileStore IP to rake attack safelist (@mkasztelnik)

### Changed
- `pressure_drops` file pattern extension changed to `dat` (@mkasztelnik)
- JWT expiration time is now the same as for other envs (@mkasztelnik)
- JWT expiration time can be configured using ENV variable (@mkasztelnik)
- `stage_in` adds commented line when file cannot be found (@mkasztelnik)
- Update to ruby 2.5.3 (@mkasztelnik)
- Update to rails 5.2.1 (@mkasztelnik)
- Replaced ERB-based templating with a Liquid-based system (@Nuanda, @mkasztelnik)

### Deprecated

### Removed

### Fixed
- Seg output shortening rule fix to deal with both success and failure outputs (@Nuanda)
- ExclusivelyOwnedGroups incorrect positive removed (@Nuanda)
- Updated truncated_off_mesh regular expression to recognize new segmentation output (@Nuanda)
- STDOUT and STDERR files reset to nil for a re-run computation (@Nuanda)
- Uploading input files via WebDAV triggers computation run (@Nuanda)
- `Pipelines::StartRunnable` starts only configured computations (@mkasztelnik)

### Security

## 0.11.0

### Added
- Patients API (@mkasztelnik)
- Accepting `file.zip` as a correct input for segmentation (@Nuanda)
- Extended clinical details section with a multi-entry widget (@Nuanda)

### Changed
- Exclude process-\* tags from the EurValve CI (@jmeizner)
- Segmentation output files have shorter names (@Nuanda)

### Deprecated

### Removed

### Fixed
- Fixed GitLab integration spec (@Nuanda)
- Missing clinical data for some patients (@Nuanda)

### Security

## 0.10.0

### Added
- User can select segmentation run mode before start (@mkasztelnik)
- Possibility to configure custom Ansys licenses for pipeline computation (@mkasztelnik)
- `pipeline_identifier` `case_number` and `token` computation script helpers (@mkasztelnik)

### Changed
- Use Gitlab review procedure instead of labels (@mkasztelnik)
- `stage_in` returns error code when unable to download FileStore file (@mkasztelnik)
- Set JWT token expiration time to 24h (@mkasztelnik)

### Deprecated

### Removed
- Computation script repositories removed from `eurvalve.yml`, step repository
  configuration used instead (@mkasztelnik)

### Fixed
- Fix unused pipeline title missing (@mkasztelnik)

### Security

## 0.9.1

### Security
- Upgrade Sprockets gem to avoid CVE-2018-3760 vulnerability (@mkasztelnik)

## 0.9.0

### Added
- Added AVD/MVD ratio to patients' statistics (@Nuanda)
- Profile link for left avatar picture and user name (@mkasztelnik)
- Colors for patient tile connected with last pipeline status (@mkasztelnik)
- Instruction how to remove concrete IP from rack-attack fail2ban list (@mkasztelnik)
- User cannot be removed or blocked when she/he owns exclusively group (@mkasztelnik)
- Button to minimize left menu is visible always (@mkasztelnik)
- Dedicated information about deleted pipeline owner (@mkasztelnik)

### Changed
- Reintegration of segmentation service using File Store in place of OwnCloud (@jmeizner)
- Remove `brakeman` from `Gemfile` and use latest version while executing
  `gitab-ci` checks (@mkasztelnik)
- Trigger segmentation start after whole input file is uploaded (@mkasztelnik)

### Deprecated

### Removed
- Remove `faker` gem and replace it with `factory_bot` sequences (@mkasztelnik)

### Fixed
- Patients' statistics work correctly when turbolink-loaded (@Nuanda)
- Fix `GITLAB_HOST` markup formatting in `README.md` (@mkasztelnik)

### Security

## 0.8.0

### Added
- Reload pipeline step status on patient view (@mkasztelnik)
- Reload segmentation status after it is started (@mkasztelnik)
- Proper handling for rimrock computation start failure (@mkasztelnik)
- Execution time updated each second for active computation (@mkasztelnik)
- Pipeline specific input (@mkasztelnik)
- Patient clinical details now includes patient's state (preop/postop) (@Nuanda)
- New statistics about the current state of EurValve's prospective cohort (@Nuanda)

### Changed
- Labels for pipelines list view improved (@mkasztelnik)
- Segmentation temp file is removed from local disc after it is transferred into
  segmentation Philips service (@mkasztelnik)
- Use stages instead of types in Gitlab CI yml (@mkasztelnik)
- Upgrade to rubocop 0.51.0 (@mkasztelnik)
- Use addressable gem to parse URLs (@mkasztelnik)
- Upgraded rails into 5.1.4 and other gems into latest supported versions (@mkasztelnik)
- Change `factory_girl` into `factory_bot` (@mkasztelnik)
- Use preconfigured docker image for builds (@mkasztelnik)
- Extracted DataSets::Client from Patients::Details for reusability (@Nuanda)
- Lock redis version into 3.x (@mkasztelnik)
- New patient case widget to reflect new developments in pipelining (@Nuanda)
- Flow model class which stores information about pipeline flow steps (@mkasztelnik)
- Switch from PhantomJS into Chrome headless (@mkasztelnik)
- Add escaping in the documentation pdp curl example (@mkasztelnik)
- Unique while fetching resources in pdp (@mkasztelnik)
- Change comparison `show` method to `index` (@mkasztelnik)
- Pipeline steps definition refactored and generalized (@mkasztelnik)
- Change defaults for data sets (@mkasztelnik)
- Change the default root path to patients index (@Nuanda)
- Renamed `not_used_flow` into `unused_steps` (@mkasztelnik)

### Deprecated

### Removed

### Fixed
- Remove n+1 query when updating computation status (@mkasztelnik)
- Sidebar's hamburger button is operational properly toggling the left menu (@dharezlak)
- Fix patient web socket unsubscribe (@mkasztelnik)
- Patient and pipeline creation silent failures (@jmeizner)

### Security

## 0.7.0

### Added
- Show started rimrock computation source link (@mkasztelnik)
- Parameter extraction pipeline step (@amber7b)
- Automatic and manual pipeline execution mode (@mkasztelnik)
- Pipeline flows (pipeline with different steps) (@jmeizner)
- Configure redis based cache (@mkasztelnik)
- Add cache for repositories tags and versions (@mkasztelnik)
- Manual gitlab-ci push master to production step (@mkasztelnik)
- Patient details are fetched from the external data set service and shown in the patient page (@dharezlak)
- Possibility to configure automatic pipeline steps during pipeline creation (@mkasztelnik)
- Wrench automatic pipeline step icon when configuration is needed (@mkasztelnik)
- Add extra pipeline steps for CFD, ROM, 0D, PV visualization and uncertainty analysis (@amber7b)
- Computation update interval can be configured using ENV variable (@mkasztelnik)
- Add loading indicator when reloading current pipeline step (@mkasztelnik)
- Show manual pipeline steps during pipeline creation (@mkasztelnik)
- External data set service is called to fetch patient's inferred details (@dharezlak)
- Additional mocked pipelines and pipelines steps (@mkasztelnik)
- Show pipeline details (flow and owner) (@Nuanda)
- Design new pipeline disabled button (@mkasztelnik)
- Links to 4 EurValve datasets query interfaces (@mkasztelnik)
- Add possibility to configure data sets site path though ENV variable (@mkasztelnik)
- Add link to segmentation status output directory (@mkasztelnik)
- Show computation error message (@mkasztelnik)
- Gitlab endpoint can be configured using ENV variable (@mkasztelnik)
- Gitlab clone url can be configured though ENV variable (@mkasztelnik)
- Add `clone_repo(repo)` script generator helper (@mkasztelnik)

### Changed
- Segmentation run mode can be configured using yaml or ENV variable (@mkasztelnik)
- Default segmentation run mode changed into 3 (@mkasztelnik)
- Patient.case_number now used as patient ID in HTTP requests (@amber7b)
- Computation update interval changed into 30 seconds (@mkasztelnik)
- Load patient details only when needed (@mkasztelnik)
- Patient's case number is now easily available in the script generator (@dharezlak)
- Fetch patient details timeout set to 2 seconds (@mkasztelnik)
- Patient details loaded using ajax (@mkasztelnik)
- Don't show run segmentation button when segmentation is active (@mkasztelnik)
- Don't show blank option for pipeline mode (@mkasztelnik)
- Patient age inferred value expressions changed to conform with values coming from real data sets (@dharezlak)
- File browsers embedded in the pipeline views sort contents by date descendingly (@dharezlak)

### Deprecated

### Removed

### Fixed
- Output from one pipeline is not taken as different pipeline input (@mkasztelnik)
- Change step status after segmentation is submitted (@mkasztelnik)
- Error when deleting a pipeline which has data files (@jmeizner)
- Set segmentation status to failure when zip cannot be unzipped (@jmeizner)
- Don't create webdav structure when pipeline cannot be created (@mkasztelnik)
- Show error details when pipeline cannot be created (@mkasztelnik)
- Fix proxy expired email title (@mkasztelnik)
- JSON query requesting inferred patient details updated to comply with data set service's new API (@dharezlak)
- Don't show run button when manual pipeline and proxy is not valid (@mkasztelnik)
- Update computation status after computation is started (@mkasztelnik)
- Extract computations_helper GitLab host name to an env variable (@Nuanda)

### Security

## 0.6.1

## Fixed
- Fix segmentation status update after WebDav computation finished (@mkasztelnik)

## 0.6.0

### Added
- rack-attack bans are logged to `log/rack-attack.log` (@mkasztelnik)
- Show user email in admin users index view (@mkasztelnik)
- Pipeline step script generation uses ERB templates (@mkasztelnik)
- Blood flow computation is self-contained - Ansys files are downloaded from git
  repository (@mkasztelnik)
- Gitlab integration services (list branches/tags and download specific file) (@amber7b)
- Comparison of image files in pipeline comparison view (@amber7b)
- Fetch computation slurm start script from Gitlab (@mkasztelnik)
- Pipeline comparison view uses OFF viewers to show 3D mesh differences (@dharezlak)
- Enabled selection of rimrock computation version (@jmeizner, @mkasztelnik)
- Brakeman security errors check is executed by Gitlab CI (@mkasztelnik)
- Data sets API reference added to the Help section as a separate entry (@dharezlak)
- Use action cable (web sockets) to refresh computation (@mkasztelnik)
- Computation stores information about selected tag/branch and revision (@mkasztelnik)
- Pipelines comparison shows sources diff link to GitLab (@Nuanda)

### Changed
- Upgrade to ruby 2.4.1 (@mkasztelnik)
- Upgrade dependencies (@mkasztelnik)
- Upgrade to rails 5.1.2 (@mkasztelnik)
- Update rubocop to 0.49.1 (@mkasztelnik)
- Move `Webdav::Client` into `app/models` to avoid auto loading problems (@mkasztelnik)
- OFF viewer's height in pipeline's compare mode takes half of the available width (@dharezlak)
- Patient case_number is checked for unsafe characters (@Nuanda)

### Deprecated

### Removed

### Fixed
- Patient left menu focus when showing patient pipeline computation (@mkasztelnik)
- Avoid n+1 queries in patient view (@mkasztelnik)
- Disable turbolinks in links to cloud view (turbolinks does not work well with GWT) (@mkasztelnik)
- Fix random failing test connected with html escaping user name in generated emails (@mkasztelnik)
- Fix content type mismatch for file stage out (@mkasztelnik)
- Turn on `files` integration tests (@mkasztelnik)
- Fix missing tooltip messages for queued computation state (@mkasztelnik)
- Preventing WebDAV folder creation for patients with incorrect case_number (@Nuanda)

### Security

## 0.5.0

### Added
- Pipelines have own DataFiles which represent produced results (@Nuanda)
- Pipelines comparison view which present differences between files of two Pipelines (@Nuanda)
- Segmentation output is unzipped into pipeline directory (@amber7b, @mkasztelnik)
- Store in FileStore heart model computation PNG results (@mkasztelnik).
- Integration tests (tag "files") are run by GitlabCI (@mkasztelnik)

### Changed
- File browser template for multiple embed mode created (@dharezlak)
- WebDAV synchronisation takes care for pipeline-related and patient input files (@Nuanda)
- Computations are executed in the pipeline scope (@mkasztelnik)
- Move FileStore configuration into one place (@mkasztelnik)
- Make patient `case_number` factory field unique (@mkasztelnik)
- Redirect user back to previous page after proxy is regenerated successfully (@mkasztelnik)
- Prefix is removed from segmentation outputs (@mkasztelnik)
- Show compare two pipelines button only when there is more than on pipeline (@mkasztelnik)

### Deprecated

### Removed
- DataFile#handle removed since it is not going to be used anymore (@Nuanda)

### Fixed
- Policy management API handles paths with escaped characters well and limits path scope to a single service (@dharezlak)
- Move and copy policy API calls handle well resources with escaped characters (@dharezlak)
- Copy and move destinations for policy management API treated as pretty paths for proper processing (@dharezlak)
- JS resources required for the OFF viewer are loaded in the file browser direct embed mode (@dharezlak)
- Resource pretty path decodes and encodes values according to URI restrictions (@dharezlak)
- Local path exclusion validation query fixed (@ddharezlak)
- Proxy warning is shown only for active Rimrock computations (@mkasztelnik)
- Correct computation tooltip text on pipelines list (@mkasztelnik)
- When removing policies via API access method existence check is scoped to a given service (@dharezlak)
- Set Rimrock computation job id to nil while restarting computation (@mkasztelnik)
- Show missing patient outputs on pipeline diff view (@mkasztelnik)
- Path processing fixed for policy move/copy operations invoked via API (@dharezlak)
- Set correct order for pipelines computations (@mkasztelnik)

### Security

## 0.4.4

### Changed
- JWT token parameter name changed to access_token for the data sets subpage request URL (@dharezlak)

## 0.4.3

### Fixed
- JWT token passed as a URL query parameter for the data sets subpage (@dharezlak)

## 0.4.2

### Changed
- JWT token structure description updated (information about `sub` record)
  in documentation (@mkasztelnik)

## 0.4.1

### Changed
- `sub` changed from integer into string (which is required by specification) (@mkasztelnik)

## 0.4.0

### Added
- Basic rack-attack configuration (@mkasztelnik)
- Patient pipelines (@mkasztelnik)
- Patient/Pipeline file store structure is created while creating/destroying
  patient/pipeline (@mkasztelnik)
- Segmentation patient case pipeline step (@tbartynski)
- User id was added into JWT token using `sub` key (@mkasztelnik)

### Changed
- Update rubocop into 0.47.1, fix new discovered offenses (@mkasztelnik)
- JWT token expiration time extended to 6 hours (@mkasztelnik)
- Change zeroclipboard into clipboard.js because support for flash is dropped (@mkasztelnik)
- Factories fields which should be unique use unique Faker methods (@mkasztelnik)
- Simplify Webdav operations for FileStore and OwnCloud (@mkasztelnik)

### Deprecated

### Removed
- PLGrid left menu removed since we are not showing any item there (@mkasztelnik)
- PLGrid patient data synchronizer removed. FileStore is the only supported backend (@mkasztelnik)

### Fixed
- Left menu can be scrolled when high is small (@mkasztelnik)
- Policy API filters policies according to the service identified by the passed id (@dharezlak)
- A 404 error code is returned instead of the 500 code when copying/moving policy
  for a non-existent source policy (@dharezlak)
- All user groups assignments are removed while user is deleted from the system (@mkasztelnik)

### Security

## 0.3.4

### Changed
- Default Patient Case File Synchronizer changed into WebDav (@mkasztelnik)

## 0.3.3

### Fixed
- Data sets page url can be overwritten with an environment property (@dharezlak)


## 0.3.2

### Fixed

- Fixed pushing tag into production from different branch than master (@mkasztelnik)


## 0.3.1

### Fixed

- Policy API filters policies according to the service identified by the passed id (@dharezlak)


## 0.3.0

### Added
- PLGrid proxy details shown in the profile view, warning shown when proxy is
  outdated while active computations are present (@mkasztelnik)
- Support for running specs with JS support (@mkasztelnik)
- When a computation finishes the patient view is automatically reloaded in order
  to show the user the latest files produced by the computation (@Nuanda)
- OD Heart model is now incorporated in the patient case pipeline (@Nuanda)
- Show alert when unable to update patient files or run computations
  because PLGrid proxy is outdated (@mkasztelnik)
- User with active computations is notified (via email) while proxy expired (@mkasztelnik)
- Users management view redesigned, added possibility to remove user by admin (@mkasztelnik)
- Set plgrid login in OpenId request while regenerating proxy certificate (@mkasztelnik)
- Long living JWT tokens can be generated using internal API (@mkasztelnik)
- Resource paths support wildcard characters through UI and API (@dharezlak)
- User can download patient case computation stdout and stderr (@mkasztelnik)
- Data sets view from an external server was integrated as an iframe (@dharezlak)

### Changed
- Make jwt pem path configurable though system variable (@mkasztelnik)
- Externalize JWT token generation from user into separate class (@mkasztelnik)
- Make PDP URI and access method case insensitive (@mkasztelnik)
- Improve service URL form placeholder (@mkasztelnik)
- Externalize PLGrid grant id into configuration file (@mkasztelnik)
- Rename `User.with_active_computations` into `User.with_submitted_computations` (@mkasztelnik)
- Corrected SMTP config to include Auth support (@jmeizner)
- Updated PDP and Services documentation (@jmeizner)
- Update rails into 5.0.2 and other dependencies (@mkasztelnik)
- Make Computation a base class for more specialized classes (@tomek.bartynski)
- Introduced WebdavComputation and RimrockComputation that inherit from Computation class (@tomek.bartynski)
- Patient Case pipeline is now hardcoded in Patient model (@tomek.bartynski)
- Implemented classes that run Blood Flow Simulation and Heart Model Computation pipeline steps (@tomek.bartynski)

### Deprecated

### Removed

### Fixed
- Correct default pundit permission denied message is returned when no custom message is defined (@mkasztelnik)
- Missing toastr Javascript map file added (@mkasztelnik)
- Show add group member and group management help only to group group owner (@mkasztelnik)
- Unify cloud resources view with other views (surrounding white box added) (@mkasztelnik)
- Fix path parsing when alias service name is used in PDP request (@mkasztelnik)
- PLGrid profile view visible only for connected accounts (@mkasztelnik)
- PDP - wildcard at the beginning and end of resource path should not be default (@mkasztelnik)

### Security


## 0.2.0

### Added
- Application version and revision in layout footer (@mkasztelnik)
- Pundit authorized error messages for groups and services (@mkasztelnik)
- Notifications are using the JS toastr library for fancier popups (@dharezlak)
- The file store component uses portal's notification system to report errors (@dharezlak)
- Service owner can manage local policies through UI (@mkasztelnik)
- Unique user name composed with full name and email (@mkasztelnik)
- Embed atmosphere UI into cloud resources section (@nowakowski)

### Changed
- Redirect status set to 302 instead of 404 (when record not found), 401 (when
  user does not have permission to perform action) to avoid ugly "You are being
  redirected" page (@mkasztelnik)
- PDP denies everything when user is not approved (@mkasztelnik)
- Add/remove group members redesigned (@mkasztelnik)
- Update rubocop and remove new offenses (@mkasztelnik)
- Update project dependencies (@mkasztelnik)
- Update rubocop to 0.46.0, remove new discovered offences (@mkasztelnik)
- Update to rails 5.0.1 (@mkasztelnik)

### Deprecated

### Removed
- Remove outdated `ResourceController` (@mkasztelnik)
- Remove support for shared session between organizations (@mkasztelnik)

### Fixed
- Service factory that used to randomly produce invalid objects (@tomek.bartynski)
- Edit/destroy group buttons visible only for group owners (@mkasztelnik)
- Administration side menu item displayed only if it is not empty (@tomek.bartynski)
- Corresponding resource entities are removed when policy API called with
  only a `path` param (@dharezlak)

### Security


## 0.1.0

### Added
- Basic project structure (@mkasztelnik, @dharezlak)
- Use [gentelalla](https://github.com/puikinsh/gentelella) theme (@dharezlak, @mkasztelnik)
- Integration with PLGrid openId (@mkasztelnik)
- Accepting new users by supervisor (@dharezlak)
- Policy Decision Point (PDP) REST API (@mkasztelnik)
- JWT Devise login strategy (@mkasztelnik)
- JWT configuration (@tomek.bartynski)
- Resource and permission management (@dharezlak)
- Patient case POC (@Nuanda)
- Sentry integration (@mkasztelnik)
- Get user gravatar (@mkasztelnik)
- Store PLGrid proxy in DB while logging in using PLGrid openId (@mkasztelnik)
- JWT token expiry set to 1 hour (@tomek.bartynski)
- Help pages for REST API (@mkasztelnik)
- Custom error pages (@mkasztelnik)
- Group hierarchies performance tests (@tomek.bartynski)
- Push patient case files into PLGrid using PLGData (@Nuanda)
- Use Rimrock for PLGrid job submission (@mkasztelnik)
- Approve user account after PLGrid login (@mkasztelnik)
- Asynchronous email sending (@mkasztelnik)
- Notify supervisors after new user registered (@mkasztelnik)
- Find resources using regular expressions (@dharezlak)
- Introduce service (@mkasztelnik)
- WebDav file browser (@dharezlak)
- REST interface for local resource management (@dharezlak)
- User profile page (@mkasztelnik)
- Project description (@amber7b, @mkasztelnik, @dharezlak)
- Sent confirmation email after user account is approved (@mkasztelnik)
- Setup continuous delivery (@tomek.bartynski)
- Setup rubocop (@tomek.bartynski)
- Service ownership (@Nuanda)
- Delegate user credentials in service resource management REST API (@mkasztelnik)
- Service management UI (@Nuanda, @mkasztelnik)
- Group management UI (@mkasztelnik)
- Show service token on show view (@mkasztelnik)
- Deploy to production when tag is pushed into remote git repository (@tomek.bartynski)
- Group hierarchies management UI (@mkasztelnik)
- Add new registered user into default groups (@mkasztelnik)
- Resource management REST API documentation (@dharezlak)
- Service aliases (@jmeizner)
- Issue and merge request templates (@mkasztelnik)
- Rake task for generating sample data for development purposes (@tomek.bartynski)
- Users can set Access Methods for new and existing Services (@Nuanda)
- There are global Access Methods which work for all Services (@Nuanda)
- Bullet gem warns about database queries performance problems (@tomek.bartynski)
- Omnipotent admin (@Nuanda)
- Global resource access policies management UI (@mkasztelnik)
- Additional attribute with policy proxy URL is passed to the file store browser (@dharezlak)
- Provide help panels for resource actions (@nowakowski)
- Hint regarding resource paths in global policies tab (@tomek.bartynski)
- Support path component in service uri (@tomek.bartynski)

### Changed
- Upgrade to Rails 5 (@mkasztelnik, @Nuanda)
- Left menu refactoring - not it is server side rendered (@mkasztelnik)
- Simplify login form pages using `simple_form` wrapper (@mkasztelnik)
- Resource separation into `global` and `local` (@dharezlak)
- Unify UIs for all CRUD views (@mkasztelnik)
- WebDav browser JS files obtained from a dedicated CDN (@dharezlak)
- Delete service ownership when user or service is destroyed (@mkasztelnik)
- Service views refactoring - separation for global/local policies management (@mkasztelnik, @Nuanda)
- Rewrite migrations to avoid using ActiveRecord (@amber7b)
- Increase fade out time for flash messages into 10 seconds (@mkasztelnik)
- Go to new user session path after logout (@mkasztelnik)
- Got to new user session path after logout (@mkasztelnik)
- Added a spec checking for correct removal of only policies specified by the API request parameter (@dharezlak)
- File store view uses a common layout (@dharezlak)
- Service uri must not end with a slash (@tomek.bartynski)
- Resource path must start with a slash (@tomek.bartynski)
- Removed path unification in Resource model (@tomek.bartynski)
- Update to sidekiq 4.2.2 and remove unused anymore sinatra dependency (@mkasztelnik)
- Administrator is able to see all registered services (@mkasztelnik)
- Fixed path field in a form for Resource (@tomek.bartynski)

### Deprecated

### Removed
- Remove webdav specific access methods from seeds (@mkasztelnik)

### Fixed
- Improve service URI validation (@Nuanda)
- Fix extra spaces in markdown code blocks (@mkasztelnik)
- Disable turbolinks on WebDav browser view to make GWT work (@mkasztelnik)
- Fix group validation - at last one group owner is needed (@mkasztelnik)
- Policies do not forbid access for access methods which they do not define (@Nuanda)
- Make icheck work with turbolinks (@mkasztelnik)
- Make zero clipboard work with turbolinks (@mkasztelnik)
- Don't allow to create group without group owner (@mkasztelnik)
- Show notice information to the user after new account created that account needs to
  be approved by supervisor (@mkasztelnik)
- Show `new` view instead of `edit` while creating new group and validation failed (@mkasztelnik)
- Remove n+1 query for service ownership in Services#index view (@Nuanda)
- Made it possible to navigate to Service#show with a click if a service had no name (@Nuanda)
- Content passed to the file store JS browser sanitized to prevent XSS attacks (@dharezlak)
- Turbolinks disabled directly on the Files link (@dharezlak)
- PDP returns 403 when `uri` or `access_method` query params are missing (@mkasztelnik)
- Fix missing translations on service list and service show views (@mkasztelnik, @Nuanda)
- Fix n+1 queries problems in service and group sections (@mkasztelnik)
- Check service policy only once on global policies view (@mkasztelnik)
- More advance validation for service `uri` overridden (@jmeizner)
- Policy management API documentation corrected (@dharezlak)
- Fix duplicates and missing part of `Service#show` (@jmeizner)
- Policy API uses a service reference when querying for access methods (@dharezlak)

### Security
