#!/usr/bin/env bash

if [ -z ${1} ]; then
cat <<- INFO
Version is not set, please use following command:

./bin/release VERSION NEXT_VERSION
INFO
exit 1
fi

if [ -z ${2} ]; then
cat <<- INFO
Next version is not set, please use following command:

./bin/release VERSION NEXT_VERSION
INFO
exit 2
fi

VERSION=$1
NEXT_VERSION=$2-pre

printf "$VERSION" > ./VERSION
sed -i "s/Unreleased/$VERSION (`date +%d.%m.%Y`)/" ./CHANGELOG.md

git add VERSION CHANGELOG.md
git commit -m "$VERSION release"
git push
git tag v$VERSION -m "$VERSION release"
git push --tags

MARKER="Please view this file on the master branch, on stable branches it's out of date."
UNRELEASED="## Unreleased\n\n### Added\n\n### Changed\n\n### Deprecated\n\n### Removed\n\n### Fixed"

printf "$NEXT_VERSION" > ./VERSION
sed -i "s/${MARKER}/${MARKER}\n\n${UNRELEASED}/" ./CHANGELOG.md

git add VERSION CHANGELOG.md
git commit -m "Next development cycle"
git push
