# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: ENV["MAIL_ADDR"] || "dice@cyfronet.pl"
  layout "mailer"
end
