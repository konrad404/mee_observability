import { Controller } from '@hotwired/stimulus'

export default class extends Controller{
    static targets = ["toggled", "specialElement"]
    static values = {classSwitchedOn: String, classSwitchedOff: String, classSwitchPrevent: String}

    toggle(){
        if(this.specialElementTarget && !this.specialElementTarget.classList.contains(this.classSwitchPreventValue)){
            if(this.toggledTarget.classList.contains(this.classSwitchedOnValue)){
                this._changeClasses(this.classSwitchedOnValue, this.classSwitchedOffValue)
            }
            else{
                this._changeClasses(this.classSwitchedOffValue, this.classSwitchedOnValue)
            }
        }
    }

    _changeClasses(classToRemove, classToAdd){
        this.toggledTarget.classList.remove(classToRemove)
        this.toggledTarget.classList.add(classToAdd)
    }
}