# frozen_string_literal: true

class GrantsValidationsJob < ApplicationJob
  def perform
    Organization.find_each do |organization|
      Organization::ValidateGrantJob.perform_later(organization)
    end
  end
end
