# frozen_string_literal: true

class FlowStep < ApplicationRecord
  belongs_to :flow
  belongs_to :step

  validates :position, presence: true, numericality: true
end
