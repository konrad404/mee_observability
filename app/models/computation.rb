# frozen_string_literal: true

class Computation < ApplicationRecord
  include ParameterValues
  include DataFiles
  include SecretControl
  include FileHandler

  attr_accessor :internal_errors

  belongs_to :user
  belongs_to :pipeline, touch: true
  belongs_to :step

  has_one_attached :stdout, service: :local
  has_one_attached :stderr, service: :local

  validates :status,
            inclusion: { in: %w[created new queued running error finished aborted] }
  validate do
    errors.merge!(internal_errors) if internal_errors
  end
  validates :script, presence: { message: "Validation error, some errors are occurring, check the configuration" }, unless: :created?

  scope :active, -> { where(status: %w[new queued running]) }
  scope :submitted, -> { where(status: %w[queued running]) }
  scope :created, -> { where(status: "created") }
  scope :not_finished, -> { where(status: %w[created new queued running]) }
  scope :for_patient_status, ->(status) { where(pipeline_step: status) }

  delegate :mode, :manual?, :automatic?, to: :pipeline
  delegate :name, :site, :organization, to: :step
  delegate :repository, :file, :host, :download_key, to: :step

  before_update :remove_secret, if: :status_changed?

  def initialize(attrs = {})
    if attrs
      step = attrs[:step]
      pipeline = attrs[:pipeline]

      attrs[:user] ||= pipeline&.user
      attrs[:pipeline_step] = step&.name
    end

    super(attrs)
  end

  def active?
    %w[new queued running].include? status
  end

  def finished?
    %w[error finished aborted].include? status
  end

  def run
    site.run(self)
  end

  def abort!
    site.abort(self)
  end

  def runnable?
    !pipeline.archived? && step.input_present_for?(pipeline)
  end

  def configured?
    tag_or_branch.present?
  end

  def success?
    status == "finished"
  end

  def error?
    status == "error"
  end

  def created?
    status == "created"
  end

  def computed_status
    if success?
      :success
    elsif error?
      :error
    elsif active?
      :running
    else
      :waiting
    end
  end

  def site_host
    site.host
  end

  def fetch_logs
    stdout.attach(io: site.fetch_logs(stdout_path, user), filename: "slurm.out")
    stderr.attach(io: site.fetch_logs(stderr_path, user), filename: "slurm.err")
  end

  def belongs_to_campaign?
    !pipeline.campaign.nil?
  end

  def tag_or_branch
    parameter_value_for(Step::Parameters::TAG_OR_BRANCH)&.value
  end

  private
    def submitted?
      %w[queued running].any?(status)
    end
end
