# frozen_string_literal: true

require "friendly_id_error_mover"

class Organization < ApplicationRecord
  extend FriendlyId
  include InputsStub

  friendly_id :name, use: [:slugged, FriendlyIdErrorMover]

  include Licenses
  include Steps
  include Git

  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships
  has_many :approved_users,
           -> { where(memberships: { state: :approved }) },
           through: :memberships,
           source: "user"

  has_many :patients, dependent: :destroy
  has_many :flows, dependent: :destroy
  has_many :grants, dependent: :destroy
  has_many :data_file_types, dependent: :destroy do
    def find_by_filename(filename)
      detect { |dft| dft.match?(filename) }
    end
  end
  has_many :pipelines, as: :runnable
  has_many_attached :artifacts
  has_many :cohorts, dependent: :destroy

  has_one_attached :logo, service: :local

  validates :name, presence: true
  validates :plgrid_team_id, presence: true
  validates :storage_quota, presence: true

  include Errorable

  def copy_slug_error_to_name
    if errors[:friendly_id].present?
      errors.add(:name, errors[:friendly_id].first)
    end
  end

  def organization
    self
  end

  def organization_id
    id
  end

  def pick_file(type)
    nil
  end

  def quota
    StorageQuota.new(self)
  end

  def validate_later
    Organization::ValidateTokenJob.perform_later(self) if git_uses_private_token?
    steps.find_each do |step|
      Step::ValidateRepositoryJob.perform_later(step)
    end
  end
end
