# frozen_string_literal: true

class Grant < ApplicationRecord
  include TimeScope, OrganizationUnit

  has_many :grant_typings, dependent: :destroy
  has_many :grant_types, through: :grant_typings

  validates :name, presence: true
  validates :grant_types, presence: true
end
