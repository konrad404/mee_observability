# frozen_string_literal: true

require "tmpdir"

class GitRepository::NativeClient < GitRepository
  def versions(force_reload: false)
    Rails.cache.fetch("git-versions/#{@host}/#{@path}", force: force_reload) do
      output, error, status = ls_remote
      if status.success?
        parse_versions(output)
      else
        error_msg = "Requested project #{@path} not found. Error messages: #{error}"
        Rails.logger.error(error_msg)
        nil
      end
    end
  end

  def content_and_revision_for(file_path, tag_or_branch)
    Dir.mktmpdir("mee-repo") do |dir|
      git_command = "
        cd #{dir};
        git clone --depth 1 --filter=blob:none --no-checkout git@#{@host.shellescape}:#{@path.shellescape};
        cd #{@path.shellescape.sub(/.*\//, '')};
        git show #{tag_or_branch.shellescape}:#{file_path.shellescape}"
      content, _, _ = run_git_command(git_command)

      git_command = "cd #{@path.shellescape.sub(/.*\//, '')};
                     git rev-parse #{tag_or_branch.shellescape}"
      revision, _, _ = run_git_command(git_command)

      [content, revision.chomp]
    end
  end

  private
    REFS_HEADS = "refs/heads/"
    REFS_TAGS = "refs/tags/"

    def parse_versions(branches_and_tags)
      refs = branches_and_tags.split(/\t|\n/)
      { branches: parse_heads(refs), tags: parse_tags(refs) }
    end

    def parse_heads(branches_and_tags)
      branches_and_tags.filter_map { |r| r.sub(REFS_HEADS, "") if r.starts_with?(REFS_HEADS) }
    end

    def parse_tags(branches_and_tags)
      branches_and_tags.filter_map { |r| r.sub(REFS_TAGS, "") if  r.starts_with?(REFS_TAGS) }
    end
end
