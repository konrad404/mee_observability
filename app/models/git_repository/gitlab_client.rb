# frozen_string_literal: true

class GitRepository::GitlabClient < GitRepository
  attr_reader :private_token

  def initialize(attrs)
    @private_token = attrs[:private_token]

    super(attrs)
  end

  def versions(force_reload: false)
    Rails.cache.fetch("gitlab-versions/#{@host}/#{@path}",
                      force: force_reload) { fetch_versions }
  rescue Gitlab::Error::NotFound
    error_msg = "Requested project #{@path} not found"
    Rails.logger.error(error_msg)
    nil
  end

  def content_and_revision_for(file_path, tag_or_branch)
    result = client.get_file(@path, file_path, tag_or_branch)
    [Base64.decode64(result.content), result.commit_id]
  rescue Gitlab::Error::NotFound
    error_msg = "Requested file #{file_path} not found in branch/tag "\
                "#{tag_or_branch} of project #{@path}"
    Rails.logger.error(error_msg)
    raise ComputationError, error_msg
  end

  private
    def fetch_versions
      rescued_invocation(branches: [], tags: [], default_branch: nil) do
        branches = client.branches(@path, per_page: 100)
        tags = client.tags(@path, per_page: 100)
        {
          branches: branches.collect(&:name),
          default_branch: branches.find(&:default)&.name,
          tags: tags.collect(&:name)
        }
      end
    end

    def client
      @client ||= Gitlab.client(endpoint:,
                                private_token: @private_token)
    end

    def endpoint
      URI::HTTPS.build(host: @host, path: "/api/v4").to_s
    end

    def rescued_invocation(default)
      yield
    rescue Gitlab::Error::MissingCredentials, Gitlab::Error::Unauthorized
      Rails.logger.error("Gitlab operation invoked with no valid credentials.")
      default
    rescue OpenSSL::SSL::SSLError, SocketError, Gitlab::Error::Parsing
      Rails.logger.error("Unable to establish Gitlab connection. Check your gitlab host config.")
      default
    end
end
