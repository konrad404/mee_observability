# frozen_string_literal: true

module User::Pipelines
  extend ActiveSupport::Concern

  included do
    has_many :computations, dependent: :nullify
  end

  class_methods do
    def with_submitted_computations
      joins(:computations)
        .where(computations: { status: [:queued, :running] })
        .distinct
    end
  end
end
