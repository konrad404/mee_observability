# frozen_string_literal: true

module User::Organizations
  extend ActiveSupport::Concern

  included do
    has_many :memberships, dependent: :destroy
    has_many :organizations, through: :memberships
  end

  class_methods do
    def approved_in(organization)
      joins(:memberships).
        where(memberships: { organization:, state: :approved })
    end
  end
end
