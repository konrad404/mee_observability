# frozen_string_literal: true


module Computation::DataFiles
  extend ActiveSupport::Concern

  def data_file(data_file_type)
    data_file_id = parameter_values.find { |pv| pv.key == "#{Parameter::RequiredDataFile::PREREQUISITE}#{data_file_type.data_type}" }&.data_file_id.to_i
    data_files = pipeline.data_files(data_file_type.data_type)
    if data_files
      data_file_id ? data_files.select { |blob| blob.id == data_file_id }.first : data_files.first
    else
      nil
    end
  end
end
