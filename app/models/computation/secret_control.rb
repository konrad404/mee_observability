# frozen_string_literal: true

module Computation::SecretControl
  extend ActiveSupport::Concern

  included do
    before_update :remove_secret, if: :status_changed?
  end

  def generate_secret
    self.secret = SecureRandom.base58
  end

  def remove_secret
    self.secret = nil if finished?
  end

  def file_manipulation?(secret)
    self.secret == secret && submitted?
  end
end
