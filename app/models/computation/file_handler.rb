# frozen_string_literal: true

module Computation::FileHandler
  extend ActiveSupport::Concern

  included do
    delegate :pick_file, to: :pipeline
  end

  def add_output(file)
    # Potential fix for: https://github.com/rails/rails/issues/42941
    blob = ActiveStorage::Blob.create_and_upload!(io: file, filename: file.original_filename, metadata: { file_type: file_type(file) })
    Rails.logger.info "Blob for #{file.original_filename} created. #{blob.id}"
    if blob && ActiveStorage::Attachment.create(name: "outputs", record_type: "Pipeline", record_id: pipeline.id, blob_id: blob.id)
      Rails.logger.info "Attached #{file.original_filename}"
      pipeline.save
      pipeline.start_computations
      Rails.logger.info "Saved pipeline and started computations"
      true
    else
      Rails.logger.error "Didn't attach file #{file.original_filename}"
      false
    end
  rescue => e
    Rails.logger.error "Failed for #{file.original_filename}, error #{e}"
  end

  def file_type(file)
    Current.organization.data_file_types
      .find_by_filename(file.original_filename)&.data_type
  end
end
