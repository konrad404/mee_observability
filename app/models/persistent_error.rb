# frozen_string_literal: true

class PersistentError < ApplicationRecord
  belongs_to :errorable, polymorphic: true, counter_cache: "persistent_errors_count"

  validates :key, presence: true
  validates :message, presence: true
end
