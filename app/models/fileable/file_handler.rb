# frozen_string_literal: true

module Fileable
  module FileHandler
    def add_input(signed_ids)
      signed_ids.each do |id|
        file = ActiveStorage::Blob.find_signed(id)
        next unless file
        remove_duplicate_attachment(file)
        file.update(metadata: { file_type: file_type(file) })
      end
      inputs.attach(signed_ids)
      start_computations
    end

    def file_type(file)
      Current.organization.data_file_types
        .find_by_filename(file.filename.to_s)&.data_type
    end

    def remove_duplicate_attachment(file)
      self.inputs.select { |attachment| attachment.filename == file.filename }.first&.purge
    end

    # it should be implemented by classes extending
    def start_computations
      raise NotImplementedError
    end
  end
end
