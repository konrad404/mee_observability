# frozen_string_literal: true

class ProgressBar::Campaign < ProgressBar::Base
  attr_reader :all

  def initialize(campaign:)
    @all = campaign.pipelines.size
    @counts = campaign.pipelines
      .group_by(&:status)
      .transform_values(&:size)
  end

  def number_of_successful
    @counts.fetch("success", 0)
  end

  def number_of_failed
    @counts.fetch("error", 0)
  end

  def number_of_waiting
    @counts.fetch("waiting", 0)
  end

  def number_of_running
    @counts.fetch("running", 0)
  end

  def tooltip_data
    I18n.t("progress_bar.data",
           waiting: number_of_waiting, running: number_of_running,
           succeeded: number_of_successful, failed: number_of_failed, all:)
  end
end
