# frozen_string_literal: true

class Artifact
  include ActiveModel::Model
  include ActiveModel::Validations
  include ActiveModel::Attributes

  attribute :filename, :string
  attribute :attachment_id, :integer

  validates :filename, presence: true

  def self.all
    Current.organization.artifacts.map { |a| Artifact.new(filename: a.filename) }
  end

  def self.find(filename:)
    if file = find_file(filename:)
      Artifact.new(filename:, attachment_id: file.id)
    end
  end

  def self.find_by_id(id)
    if file = Current.organization.artifacts.find(id).blob
      Artifact.new(attachment_id: id, filename: file.filename)
    end
  end

  def self.create(filename:, attachment_id:)
    artifact = Artifact.new(filename:, attachment_id:)
    artifact.save
  end

  def self.attach(signed_ids)
    signed_ids.map do |id|
      filename = ActiveStorage::Blob.find_signed(id)&.filename&.to_s
      Artifact.find(filename:)&.delete
    end
    Current.organization.artifacts.attach(signed_ids)
  end

  def self.upload(files:)
    files.each do |file|
      artifact = Artifact.new(filename: file.original_filename)
      artifact.do_save(file) if artifact.valid?
    end
    true
  end

  def save
    if validate
      io = StringIO.new(attachment.blob.download)
      do_save(io)
    end
  end

  def do_save(io)
    remove_duplicate
    Current.organization.artifacts.attach(io:, filename:)
    self.attachment_id = Artifact.find_file(filename:).id
    self
  end

  def update(filename:)
    self.filename = filename
    if validate && unique?(filename)
      self.filename = filename
      attachment.blob.update(filename:)
      self
    end
  end

  def delete
    attachment.purge
  end

  def attachment
    ActiveStorage::Attachment.find(attachment_id)
  end

  private
    def self.find_file(filename:)
      Current.organization.artifacts_blobs.find_by(filename:)&.attachments&.take
    end

    def remove_duplicate
      Artifact.find(filename:)&.delete
    end

    def unique?(name)
      if Artifact.find_file(filename: name)
        errors.add(:filename, "has already been taken", value: name)
        return false
      end
      true
    end
end
