# frozen_string_literal: true

class CohortPatient < ApplicationRecord
  belongs_to :patient
  belongs_to :cohort
end
