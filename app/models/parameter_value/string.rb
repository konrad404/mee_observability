# frozen_string_literal: true

class ParameterValue::String < ParameterValue
  store_accessor :value_store, :value

  validates :value, presence: true

  def permitted_params
    [:value]
  end

  def value
    super || parameter&.default_value
  end

  def blank?
    value.blank?
  end
end
