# frozen_string_literal: true

class ParameterValue::RequiredDataFile < ParameterValue
  store_accessor :value_store, :data_file_id, :data_file_type_id

  attribute :data_file_id, :integer
  validates :data_file_id, presence: true

  def initialize(attrs)
    attrs[:data_file_type_id] = attrs["parameter"].data_file_type.id if attrs["parameter"]

    super(attrs)
  end

  def permitted_params
    [:data_file_id]
  end

  def data_files
    parameter.data_files
  end

  attr_writer :parameter
  def parameter
    @parameter ||= load_parameter
  end

  def value
    ActiveStorage::Blob.find_by(id: data_file_id.to_i).filename.to_s
  end

  def blank?
    data_file_id.blank?
  end

  def to_s
    data_file_id
  end

  private
    def load_parameter
      if @dft = DataFileType.find_by(id: data_file_type_id)
        data_files = [pipeline_inputs, pipeline_outputs, patient_inputs]
        Parameter::RequiredDataFile.new(data_file_type: @dft, data_files:)
      end
    end

    def pipeline_inputs
      ["Pipeline inputs", computation.pipeline.inputs_blobs.select { |blob| blob.metadata[:file_type] == @dft.data_type }]
    end

    def pipeline_outputs
      ["Pipeline outputs", computation.pipeline.outputs_blobs.select { |blob| blob.metadata[:file_type] == @dft.data_type }]
    end

    def patient_inputs
      ["Patient inputs", computation.pipeline.runnable_blobs.select { |blob| blob.metadata[:file_type] == @dft.data_type }]
    end
end
