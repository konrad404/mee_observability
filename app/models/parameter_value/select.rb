# frozen_string_literal: true

class ParameterValue::Select < ParameterValue
  store_accessor :value_store, :value

  validates :value, inclusion: {
    in: ->(obj) { obj.values&.map(&:value) }
  }, if: :parameter

  delegate :values, to: :parameter, allow_nil: true

  def permitted_params
    [:value]
  end

  def value
    super || parameter&.default_value
  end

  def blank?
    value.blank?
  end
end
