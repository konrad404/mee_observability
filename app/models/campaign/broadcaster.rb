# frozen_string_literal: true

module Campaign::Broadcaster
  extend ActiveSupport::Concern

  included do
    after_save do
      update_frontend if status_previously_changed?
    end

    after_update do
      broadcast_update target: "run_button", partial: "campaigns/run_buttons/#{self.status}",
        locals: { campaign: self, button_text: I18n.t("campaigns.view.run.#{status}") }
      broadcast_replace target: dom_id(self, "status"), partial: "campaigns/status", locals: { campaign: self }
    end
  end

  def pipeline_created(pipeline)
    broadcast_append target: dom_id(self, "patients"), partial: "campaigns/patient_table/pipeline_row",
                     locals: { pipeline: } if Pipeline.where(id: pipeline.id).exists?
    broadcast_update target: "run_button", partial: "campaigns/run_buttons/#{status}",
                     locals: { campaign: self, button_text: I18n.t("campaigns.view.run.#{status}") } unless pipelines_ready?
    update_status!
  end

  private
    def pipelines_ready?
      pipelines.size == created_pipelines_size
    end

    def created_pipelines_size
      desired_pipelines - failed_pipelines
    end

    def update_frontend
      update_progress_bar_card
      update_progress_bar
    end

    def update_progress_bar
      broadcast_update target: "progress_bar_campaign_#{id}", partial: "progress_bar/campaign",
                       locals: ProgressBar::Campaign.new(campaign: self).data
    end

    def update_progress_bar_card
      broadcast_update target: "progress_bar_campaign_card_#{id}", partial: "campaigns/campaign_progress_bar_card",
                       locals: { campaign: self }
    end
end
