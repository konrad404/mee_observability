# frozen_string_literal: true

class NullCohort
  def name
    "[removed]"
  end
end
