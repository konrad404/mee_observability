# frozen_string_literal: true

class Cohort < ApplicationRecord
  has_many :cohort_patients, dependent: :destroy
  has_many :patients, through: :cohort_patients

  has_many :campaigns, dependent: :nullify

  belongs_to :organization, default: -> { Current.organization }

  validates :name, presence: true,
            uniqueness: { scope: :organization_id, case_sensitive: true },
            format: { with: /\A[a-zA-Z0-9_]+\z/,
                     message: I18n.t("cohorts.name_error") }
end
