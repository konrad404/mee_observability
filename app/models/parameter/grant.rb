# frozen_string_literal: true

class Parameter::Grant < Parameter
  delegate :active_grants, to: :step

  def protected?
    true
  end
end
