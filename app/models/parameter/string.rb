# frozen_string_literal: true

class Parameter::String < Parameter
  store_accessor :config, :default_value
end
