# frozen_string_literal: true

class Parameter::RequiredDataFile
  PREREQUISITE = "prerequisite#"

  attr_reader :data_file_type, :data_files

  def initialize(data_file_type:, data_files:)
    @data_file_type = data_file_type
    @data_files = data_files
  end

  def value_class
    ParameterValue::RequiredDataFile
  end

  def name
    data_file_type.name
  end

  def hint
  end

  def key
    "#{PREREQUISITE}#{data_file_type.data_type}"
  end
end
