# frozen_string_literal: true

class Campaigns::Create
  def initialize(step_attributes, campaign_params, pipeline_params, user)
    @step_attributes = step_attributes
    @campaign_params = campaign_params
    @pipeline_params = pipeline_params
    @user = user
  end

  def call
    @campaign = Campaign.new(@campaign_params.merge(flow: @pipeline_params[:flow_id]))
    if validate_step_attributes(@step_attributes) && @campaign.save
      Campaigns::CreatePipelines.new(@campaign, @user, @pipeline_params, @step_attributes).call
    end

    @campaign
  end

  private
    def validate_step_attributes(step_attributes)
      if step_attributes.values.flat_map(&:values).flat_map(&:values).any?(&:empty?)
        @campaign.errors.add(:base, :invalid, message: I18n.t("campaigns.errors.steps_attributes"))
      end
      @campaign.errors.empty?
    end
end
