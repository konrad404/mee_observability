# frozen_string_literal: true

class Step::ValidateRepository
  BAD_KEY_MESSAGE = " or SSH key cannot provide access"
  BAD_TOKEN_MESSAGE = " or token key cannot provide access"

  def initialize(step)
    @step = step
  end

  def call
    if repo
      clear_persistent_errors
      check_repository
    end
  end

  private
    def clear_persistent_errors
      @step.persistent_errors.destroy_all
    end

    def check_repository
      @step.validate

      error_message = ""
      error_message += BAD_KEY_MESSAGE unless repo.key_valid?
      error_message += BAD_TOKEN_MESSAGE if private_token_error?
      if error_message.present?
        error_message = "is invalid" + error_message
        @step.persistent_errors.create(key: "repository", message: error_message)
      end
    end

    def repo
      @repo ||= @step&.git_repository
    end

    def private_token_error?
      (@step.git_config || @step.organization.git_config)
        .errors[:private_token].present?
    end
end
