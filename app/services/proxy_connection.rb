# frozen_string_literal: true

module ProxyConnection
  def self.build(url, proxy)
    Faraday.new(url:) do |faraday|
      faraday.request :url_encoded
      faraday.adapter Faraday.default_adapter
      faraday.headers["PROXY"] = proxy
    end
  end
end
