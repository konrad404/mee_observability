# frozen_string_literal: true

require "base64"
require "faraday"

class ProxyService
  def initialize(user, service_url, options = {})
    @user = user
    @service_url = service_url
    @proxy = user.proxy.encode
    @connection = options[:connection]
  end


  private
    def connection
      @connection ||= ProxyConnection.build(@service_url, @proxy)
    end
end
