# frozen_string_literal: true

class GuardedProxyExecutor
  def initialize(user)
    @user = user
  end

  def call
    @user.proxy.valid? ? block_given? && yield : report_problem
  end

  private
    def report_problem
      return unless notify_user?

      Notifier.proxy_expired(@user).deliver_later
      @user.update(proxy_expired_notification_time: Time.zone.now)
    end

    def notify_user?
      @user.proxy_expired_notification_time.blank? ||
        @user.proxy_expired_notification_time < 1.day.ago
    end
end
