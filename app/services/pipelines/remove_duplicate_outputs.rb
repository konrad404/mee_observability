# frozen_string_literal: true

class Pipelines::RemoveDuplicateOutputs
  attr_reader :pipeline

  def initialize(pipeline)
    @pipeline = pipeline
  end

  def call
    Pipeline.transaction { remove_duplicate_outputs }
    @pipeline
  end

  def remove_duplicate_outputs
    sorted_attachments = sort_attachments_by_filename(pipeline)
    delete_duplicates(sorted_attachments)
  end

  # Groups attachments by filename, ids store ids of attachments excluding newest
  def sort_attachments_by_filename(pipeline)
    sorted_attachments = {}
    pipeline.outputs.includes(:blob).map do |output|
      filename = output.filename.to_s
      sorted_attachments[filename] ||= {}

      if sorted_attachments[filename][:newest].nil?
        sorted_attachments[filename][:newest] = output
        sorted_attachments[filename][:ids] = []
        next
      end

      if output.created_at.before? sorted_attachments[filename][:newest].created_at
        sorted_attachments[filename][:ids] << output.id
      else
        sorted_attachments[filename][:ids] << sorted_attachments[filename][:newest].id
        sorted_attachments[filename][:newest] = output
      end
    end
    sorted_attachments
  end

  def delete_duplicates(sorted_attachments)
    sorted_attachments.map do |_, attachments|
      pipeline.outputs.where(id: attachments[:ids]).map(&:purge_later)
    end
  end
end
