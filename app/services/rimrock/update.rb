# frozen_string_literal: true

module Rimrock
  class Update < Rimrock::Service
    include ActionView::RecordIdentifier
    def initialize(user, options = {})
      super(user, options)

      @user = user
      @on_finish_callback = options[:on_finish_callback]
      @updater = options[:updater]
    end

    def call
      return if active_computations.empty?

      response = connection.get("api/jobs") do |req|
        req.options.params_encoder = Faraday::FlatParamsEncoder
        req.params = { format: "short", job_id: active_computations.map(&:job_id) }
      end

      case response.status
      when 200 then success(response.body)
      when 422 then error(response.body, :timeout)
      else error(response.body, :internal)
      end
    end

    private
      def success(body)
        statuses = JSON.parse(body).index_by { |e| e["job_id"] }
        active_computations.each do |computation|
          update_computation(computation, statuses[computation.job_id])
        end
      end

      def update_computation(computation, new_status)
        if new_status
          current_status = computation.status
          updated_status = new_status["status"].downcase
          computation.update(status: updated_status)
          if computation.finished?
            computation.fetch_logs
            Pipelines::RemoveDuplicateOutputsJob.perform_later(computation.pipeline)
          end
          on_finish_callback(computation) if computation.status == "finished"
          update(computation) if current_status != updated_status && computation.pipeline.campaign_id.nil?
        else
          computation.update(status: "error", error_message: "Job cannot be found")
        end
      end

      def error(body, error_type)
        Rails.logger.tagged(self.class.name) do
          Rails.logger.warn(
            I18n.t("rimrock.update.#{error_type}", user: @user&.name, details: body)
          )
        end
      end

      def active_computations
        @active_computations ||= @user.computations.submitted
      end

      def on_finish_callback(computation)
        @on_finish_callback&.new(computation)&.call
      end

      def update(computation)
        @updater&.new(computation)&.call
      end
  end
end
