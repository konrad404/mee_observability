# frozen_string_literal: true

module Rimrock
  class Service < ProxyService
    def initialize(user, options = {})
      super(user, url, options)
    end

    protected
      attr_reader :user

      def url
        Rails.application.config.constants.dig(:rimrock, :url)
      end

      def tag
        Rails.application.config.constants.dig(:rimrock, :tag)
      end
  end

  class Exception < RuntimeError
  end
end
