# frozen_string_literal: true

class GitlabTokenValidator < ActiveModel::EachValidator
  TEST_REPOSITORY = "cyfronet/mee"

  def validate_each(record, attribute, value)
    if host(record).present? && private_token(record).present?
      begin
        endpoint = URI::HTTPS.build(host: host(record), path: "/api/v4")

        Gitlab
          .client(endpoint: endpoint.to_s, private_token: private_token(record))
          .branches(repository(record))
      rescue Gitlab::Error::Forbidden
        record.errors.add(
          attribute,
          "has to low privileges. At least 'read_api' role is needed"
        )
      rescue StandardError
        record.errors.add(attribute, "is invalid")
      end
    end
  end

  private
    def host(record)
      record.host || record.try(:organization_git_config)&.host
    end

    def private_token(record)
      record.private_token || record.try(:organization_git_config)&.private_token
    end

    def repository(record)
      record.try(:repository) || TEST_REPOSITORY
    end
end
