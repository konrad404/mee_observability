# frozen_string_literal: true

class Step::InputsComponent < ViewComponent::Base
  def initialize(step:)
    @inputs = step.required_file_types
  end

  def render?
    @inputs.present?
  end
end
