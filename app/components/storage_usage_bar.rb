# frozen_string_literal: true

class StorageUsageBar < ViewComponent::Base
  attr_reader :quota, :color

  def initialize
    @quota = Current.organization.quota
    @color = quota.exceeded? ? ".bg-danger" : ".bg-primary"
  end
end
