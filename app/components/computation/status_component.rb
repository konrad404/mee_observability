# frozen_string_literal: true

class Computation::StatusComponent < ViewComponent::Base
  with_collection_parameter :computation

  def initialize(computation:)
    @computation = computation
  end

  def call
    if archived?
      inner_i = helpers.tag.i(class: icon_class, title: tooltip)
      inner_span = helpers.content_tag(:span, "!",  class: "fa-layers-counter status-with-warning")
      helpers.content_tag(:span, inner_i + inner_span, class: "fa-layers")
    else
      helpers.tag.i(class: icon_class, title: tooltip)
    end
  end

  private
    def icon_class
      if need_configuration?(@computation)
        "fas fa-wrench"
      elsif !@computation.runnable? && !archived?
        "far fa-circle"
      elsif @computation.active?
        "fas fa-circle-notch fa-spin"
      elsif @computation.status == "finished"
        "far fa-check-circle"
      elsif @computation.status == "error"
        "far fa-times-circle"
      else
        "fas fa-circle"
      end
    end

    def need_configuration?(computation)
      @computation.pipeline.automatic? && @computation.tag_or_branch.blank?
    end

    def tooltip
      if @computation.runnable?
        I18n.t("steps.#{@computation.status}", step: @computation.step.name)
      elsif @computation.pipeline.archived?
        I18n.t("steps.readonly", step: @computation.step.name)
      else
        I18n.t("steps.missing_input", step: @computation.step.name)
      end
    end

    def archived?
      @computation.pipeline.archived?
    end
end
