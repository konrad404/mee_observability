# frozen_string_literal: true

class Computation::ParametersValuesComponent < ViewComponent::Base
  def initialize(computation:)
    @parameters_values = computation.parameter_values
  end

  def parameters_values?
    @parameters_values.size.positive?
  end
end
