# frozen_string_literal: true

class ComputationError < StandardError
end
