# frozen_string_literal: true

class NotAuthenticatedError < NameError; end
