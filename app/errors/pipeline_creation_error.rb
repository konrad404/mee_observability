# frozen_string_literal: true

class PipelineCreationError < StandardError; end
