# frozen_string_literal: true

class SubdomainsController < ActionController::Base
  def redirect
    @organization = Organization.find_by slug: request.subdomain

    if @organization
      url = "#{root_url(subdomain: nil)}#{OrganizationSlug.encode(@organization.id)}/#{params[:path]}"
      redirect_to url, allow_other_host: true
    else
      redirect_to root_url(subdomain: nil),
                  allow_other_host: true,
                  alert: "Organization not found"
    end
  end

  def path
    id = OrganizationSlug.encode(@organization.id)
    "#{id}/#{params[:path] || ''}"
  end
end
