# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Sentryable

  include Authenticate
  include Pundit::Authorization
  include Organization::Authorize

  include ErrorRescues
  include HelpItems

  include Pagy::Backend
end
