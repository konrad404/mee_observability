# frozen_string_literal: true

class ArtifactsController < ApplicationController
  def index
    authorize(Artifact)
    @pagy, @artifacts = pagy(Current.organization.artifacts)
  end

  def upload
    Artifact.attach(params[:artifact][:files])
    redirect_back fallback_location: root_path, notice: t(".success")
  rescue StandardError
    redirect_back fallback_location: root_path, alert: t(".failure")
  end
end
