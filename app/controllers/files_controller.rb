# frozen_string_literal: true

class FilesController < ApplicationController
  before_action :find_and_authorize

  def destroy
    if @file.purge_later
      respond_to do |format|
        format.turbo_stream { flash.now[:notice] = t(".success") }
      end
    else
      flash.now[:alert] = t(".failure")
    end
  end

  def find_and_authorize
    @file = ActiveStorage::Attachment.find(params[:id])
    authorize(@file)
  end
end
