# frozen_string_literal: true

class ParametersController < ApplicationController
  def show
    authorize(Parameter)
    parameter_class = Parameter.by_type(params[:id])

    if parameter_class
      step = Step.new
      parameter = step.parameters.build(type: parameter_class.to_s)

      render partial: "wrapper",
        layout: false,
        locals: { step:, parameter:, id: "new-#{Time.now.to_f}" }
    else
      render json: { error: "Parameter type not found" }, status: 404
    end
  end
end
