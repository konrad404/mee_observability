# frozen_string_literal: true

class Computations::ArtifactsController < Computations::ApiController
  def show
    if artifact = Current.organization.artifacts_blobs.find_by(filename: "#{params[:name]}.#{(params[:format])}")
      redirect_to url_for(artifact)
    else
      render nothing: true, status: :not_found
    end
  end
end
