# frozen_string_literal: true

class Computations::FilesController < Computations::ApiController
  def show
    if file = @computation.pick_file(params[:type])
      redirect_to url_for(file)
    else
      render nothing: true, status: :not_found
    end
  end

  def create
    if params[:file].nil?
      render nothing: true, status: :bad_request
      return
    end

    if @computation.add_output(params[:file])
      render nothing: true, status: :created
    else
      render nothing: true, status: :internal_server_error
    end
  end
end
