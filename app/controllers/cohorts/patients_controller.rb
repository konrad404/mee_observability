# frozen_string_literal: true

class Cohorts::PatientsController < ApplicationController
  before_action :find_and_authorize, only: [:create, :destroy]
  before_action :load_patient, only: [:create, :destroy]

  def index
    @cohort = policy_scope(Cohort.includes(:patients)).find(params[:cohort_id])
    authorize(@cohort)
    @candidates = candidates
  end

  def create
    @cohort_patient = CohortPatient.new(cohort: @cohort, patient: @patient)

    unless @cohort_patient.save
      flash.now[:alert] = I18n.t("cohorts.patients.create.failed", name: @patient.case_number, cohort_name: @cohort.name)
    end
  end

  def destroy
    @cohort_patient = CohortPatient.find_by(cohort: @cohort, patient: @patient)
    if !@cohort_patient || !@cohort_patient.destroy
      flash.now[:alert] = I18n.t("cohorts.patients.destroy.failed", name: @patient.case_number, cohort_name: @cohort.name)
    end
  end

  private
    def find_and_authorize
      @cohort = policy_scope(Cohort.includes(:patients)).find_by!(id: params[:cohort_id])
      authorize(@cohort, :update?)
    end

    def load_patient
      @patient = policy_scope(Patient).find_by!(slug: params[:id])
    end

    def candidates
      policy_scope(Patient.includes(:pipelines))
        .where.not(id: @cohort.patients)
    end
end
