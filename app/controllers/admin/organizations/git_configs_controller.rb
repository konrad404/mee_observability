# frozen_string_literal: true

module Admin
  class Organizations::GitConfigsController < ApplicationController
    include GitConfigs

    before_action :find_and_authorize

    private
      def find_and_authorize
        @git_config = Current.organization.git_config
        authorize(Current.organization, :update?)
      end

      def module_type
        "organization"
      end
  end
end
