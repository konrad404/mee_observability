# frozen_string_literal: true

module Step::Params
  extend ActiveSupport::Concern

  private
    def step_params(step = Step.new)
      permitted_attributes = policy(step).permitted_attributes +
        [parameters_attributes: permitted_parameters_attributes(step)] +
        [git_config_attributes: permitted_git_config_attributes]

      params.require(:step).permit(permitted_attributes).tap do |p|
        # make sure that git_config can be removed
        p["git_config_attributes"] ||= nil
      end
    end

    def permitted_parameters_attributes(step)
      unsafe_parameters_attributes.filter_map do |id, attrs|
        parameter_class = step.parameter_class(attrs)

        [id, policy(parameter_class).permitted_attributes] if parameter_class
      end.to_h
    end

    def permitted_git_config_attributes
      git_type = params.dig(:step, :git_config_attributes, :git_type)
      git_config_class = GitConfig.by_type(git_type)

      policy(git_config_class).permitted_attributes if git_config_class
    end

    def unsafe_parameters_attributes
      params.dig(:step, :parameters_attributes)&.to_unsafe_h || {}
    end
end
