# frozen_string_literal: true

module GitConfigs
  extend ActiveSupport::Concern

  def show
    if git_config_class
      if @git_config.present?
        git_config = @git_config.change_to_class(git_config_class)
      else
        git_config = git_config_class.new
      end

      render_details(git_config)
    else
      render json: { error: "Git config type not found" }, status: 404
    end
  end

  private
    def render_details(git_config)
      render partial: "git_configs/details",
             layout: false,
             locals: { git_config:, module_type: }
    end

    def git_config_class
      GitConfig.by_type(params[:id])
    end

    def module_type
      raise "Need to be implemented"
    end
end
