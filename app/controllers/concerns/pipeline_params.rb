# frozen_string_literal: true

module PipelineParams
  extend ActiveSupport::Concern

  private
    def permitted_step_attributes(params)
      steps = flow_steps(permitted_attributes(Pipeline)[:flow_id]).includes(:parameters)
      permitted_parameter_values = steps.to_h do |step|
        [step.slug, permitted_step_parameters(step)]
      end

      params.require(:pipeline).permit(
        parameters_values: permitted_parameter_values)[:parameters_values]
    end

    def permitted_step_parameters(step)
      step.parameters.to_h do |p|
        [p.key, p.value_class.new(parameter: p).permitted_params]
      end
    end

    def flow_steps(flow_id)
      policy_scope(Step)
        .joins(:flow_steps)
        .where(flow_steps: { flow_id:  })
    end

    def remove_blank_parameters_values(pipeline)
      pipeline.computations.each do |c|
        c.parameter_values = c.parameter_values.reject(&:blank?)
      end
    end
end
