# frozen_string_literal: true

class Pipelines::FilesController < ApplicationController
  def show
    @pipeline = Pipeline.find(params[:pipeline_id])
    @computation = Computation.find(params[:computation_id])
    authorize(@pipeline)
    @runnable = @pipeline.runnable

    if request.xhr?
      render partial: "pipelines/file_browsers",
             locals: { runnable: @runnable, pipeline: @pipeline, computation: @computation },
             layout: false
    end
  end

  def create
    pipeline = Pipeline.find(params[:pipeline_id])
    pipeline.add_input(params[:pipeline][:files])
    redirect_back fallback_location: root_path, notice: t(".success")
  rescue StandardError
    redirect_back fallback_location: root_path, alert: t(".failure")
  end
end
