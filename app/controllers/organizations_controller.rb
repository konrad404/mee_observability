# frozen_string_literal: true

class OrganizationsController < RootApplicationController
  include Organization::Params
  before_action :authenticate_user!

  def new
    @organization = Organization.new(git_config: GitConfig::Gitlab.new)
    authorize(@organization)
  end

  def create
    @organization = Organization.new(organization_params)
    authorize(@organization)

    @organization.memberships
      .build(user: Current.user, state: :approved, roles: [:admin])

    if @organization.save
      redirect_to root_path
    else
      render :new, status: :unprocessable_entity
    end
  end
end
