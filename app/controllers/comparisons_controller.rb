# frozen_string_literal: true

class ComparisonsController < ApplicationController
  before_action :check_pipelines, only: [:index]
  before_action :find_and_authorize, only: [:index]
  before_action :runnable, only: [:index]

  def index
    pipelines.reload

    @first = pipelines.first
    @second = pipelines.second

    @sources =
      @first.computations.select(&:revision).filter_map do |compared_comp|
        compare_to_comp =
          @second.computations.select(&:revision).
            detect { |c| c.pipeline_step == compared_comp.pipeline_step }

        [compared_comp, compare_to_comp] if compare_to_comp
      end


    @data = { compared: [], not_comparable: [] }
    @first.outputs.each do |compared|
      compare_to = @second.outputs.detect { |attachment| compared.filename == attachment.filename }
      if comparable?(compared) && compare_to.present?
        @data[:compared] << [compared, compare_to]
      elsif compare_to.present? && compared.blob.metadata["file_type"]
        @data[:not_comparable] << compared.blob.metadata["file_type"]
      end
    end
  end

  private
    def pipelines
      @pipelines ||= Pipeline.where(id: params[:pipeline_ids]).
                     includes(computations: [:step, :parameter_values])
    end

    def runnable
      @runnable ||= @pipelines.first.runnable
    end

    def find_and_authorize
      pipelines.each { |pipeline| authorize(pipeline, :compare?) }
    end

    def check_pipelines
      if pipelines.size != 2
        redirect_to helpers.runnable_path(@runnable), alert: I18n.t("comparisons.index.invalid")
      end
    end

    def comparable?(attachment)
      DataFileType.find_by(data_type: attachment.blob.metadata["file_type"])&.viewer.present?
    end
end
