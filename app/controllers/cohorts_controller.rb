# frozen_string_literal: true

class CohortsController < ApplicationController
  before_action :find_and_authorize, only: [:show, :destroy]

  def index
    authorize(Cohort)
    @pagy, @cohorts = pagy(policy_scope(Cohort))
  end

  def new
    @cohort = Cohort.new
    authorize(@cohort)
  end

  def create
    @cohort = Cohort.new(permitted_attributes(Cohort))

    if @cohort.save
      redirect_to @cohort, notice: I18n.t("cohorts.create.success")
    else
      render :new, status: :unprocessable_entity
    end
  end

  def show
    redirect_to cohort_patients_path(@cohort)
  end

  def destroy
    if @cohort.destroy
      redirect_to cohorts_path,
                  notice: I18n.t("cohorts.destroy.success", name: @cohort.name),
                  status: :see_other
    else
      render :show,
             notice: I18n.t("cohorts.destroy.failure", name: @cohort.name)
    end
  end

  private
    def find_and_authorize
      @cohort = Cohort.find(params[:id])
      authorize(@cohort)
    end
end
