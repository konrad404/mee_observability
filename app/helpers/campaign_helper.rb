# frozen_string_literal: true

module CampaignHelper
  def show_progress_bar?(campaign)
    !campaign.creating_pipelines_failed? && !campaign.initializing?
  end

  def ready_pipelines_ratio(campaign)
    (((campaign.pipelines.size.to_f + campaign.failed_pipelines) / campaign.desired_pipelines.to_f) * 100).to_i
  end

  def result_error(campaign)
    if campaign.pipelines.size != campaign.desired_pipelines
      I18n.t("campaigns.view.pipeline_loader.partial_error",
                    number: campaign.failed_pipelines, all: campaign.desired_pipelines)
    else
      I18n.t("campaigns.view.pipeline_loader.full_error",
             ratio: "#{campaign.failed_pipelines}/#{campaign.cohort.patients.size}",
             percentage: Percentage.new(campaign.failed_pipelines, campaign.cohort.patients.size).to_i)
    end
  end
end
