# frozen_string_literal: true

class DropzoneFileInput < SimpleForm::Inputs::FileInput
  def dropzone_input(wrapper_options = nil)
    merged_input_options = merge_wrapper_options(input_html_options,
                                                 wrapper_options)

    @builder.file_field(attribute_name, merged_input_options)
  end

  def input_html_options
    super.merge({ direct_upload: true, multiple: true, data: { dropzone_target: "input" } })
  end

  def dropzone_placeholder(wrapper_options = nil)
    template.content_tag(:div, class: "dropzone-msg dz-message needsclick text-gray-600") do
      image + title + description
    end
  end

  private
    def image
      template.content_tag(:span, class: "flex justify-center mb-2") do
        options[:image] || template.image_tag("dropzone_placeholder.svg")
      end
    end

    def title
      template.content_tag(:h3, class: "dropzone-msg-title") do
        options[:title] || "Drag and drop files here"
      end
    end

    def description
      template.content_tag(:span, class: "dropzone-msg-desc text-sm") do
        options[:description] || ""
      end
    end
end
