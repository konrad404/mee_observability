# frozen_string_literal: true

class DataFileTypePolicy < ApplicationPolicy
  def index?
    admin?
  end

  def new?
    admin?
  end

  def create?
    admin? && in_organization?
  end

  def edit?
    admin? && in_organization?
  end

  def update?
    admin? && in_organization?
  end

  def destroy?
    admin? && in_organization?
  end

  def permitted_attributes
    [:data_type, :name, :pattern, :viewer]
  end

  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:)
    end
  end
end
