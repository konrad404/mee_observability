# frozen_string_literal: true

class FlowPolicy < ApplicationPolicy
  def index?
    user.organizations.include?(organization)
  end

  def new?
    in_organization?
  end

  def create?
    in_organization?
  end

  def edit?
    (admin? || owner?) && in_organization?
  end

  def update?
    (admin? || owner?) && in_organization?
  end

  def destroy?
    (admin? || owner?) && in_organization?
  end

  def permitted_attributes
    [:name, flow_steps_attributes: [:step_id, :position, :_destroy, :id]]
  end

  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:).kept
    end
  end

  private
    def owner?
      record.user == user
    end
end
