# frozen_string_literal: true

class PipelinePolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    true
  end

  def create?
    in_organization? && organization_flow?
  end

  def show?
    true
  end

  def edit?
    (admin? || owned?) && !in_campaign?
  end

  def update?
    (admin? || owned?) && !in_campaign?
  end

  def destroy?
    (admin? || owned?) && !in_campaign?
  end

  def compare?
    true
  end

  def permitted_attributes_for_create
    [:name, :flow_id, :mode, :notes]
  end

  def permitted_attributes_for_update
    [:name, :notes]
  end

  private
    def in_organization?
      record.runnable&.organization_id == organization.id
    end

    def organization_flow?
      !record.flow || record.flow.organization_id == organization.id
    end

    def owned?
      record.user == user
    end

    def in_campaign?
      record.campaign
    end
end
