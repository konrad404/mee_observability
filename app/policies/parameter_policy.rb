# frozen_string_literal: true

class ParameterPolicy < ApplicationPolicy
  def show?
    admin?
  end
end
