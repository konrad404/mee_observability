# frozen_string_literal: true

class OrganizationPolicy < ApplicationPolicy
  def permitted_attributes
    [:name, :description, :logo, :plgrid_team_id, :site_id]
  end

  def new?
    user_context.presence && admin?
  end

  def create?
    user_context.presence && admin?
  end

  def show?
    admin?
  end

  def update?
    admin?
  end

  def destroy?
    admin?
  end
end
