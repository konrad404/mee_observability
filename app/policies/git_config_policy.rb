# frozen_string_literal: true

class GitConfigPolicy < ApplicationPolicy
  def new?
    @user_context.present?
  end

  def show?
    admin?
  end
end
