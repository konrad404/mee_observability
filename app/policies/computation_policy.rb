# frozen_string_literal: true

class ComputationPolicy < ApplicationPolicy
  def permitted_attributes
    [parameter_values_attributes: parameter_attributes]
  end

  def show?
    true
  end

  def update?
    !Current.organization.quota.exceeded? &&
      record.user == user && !record.active? && can_update_in_mode? &&
      record.step.persistent_errors_count.zero?
  end

  def runnable?
    !Current.organization.quota.exceeded? &&
      @user_proxy.valid? && record.runnable? && record.configured?
  end

  def abort?
    record.user == user && record.active? && record.manual?
  end

  private
    def parameter_attributes
      record.form_parameters_values.to_h do |pv|
        [pv.key, pv.permitted_params]
      end
    end

    def can_update_in_mode?
      if record.manual?
        record.runnable?
      else
        record.step.parameters.size != record.parameter_values.size
      end
    end
end
