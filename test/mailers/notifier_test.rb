# frozen_string_literal: true

require "test_helper"

class NotifierTest < ActionMailer::TestCase
  test "account approved: notify user" do
    email = Notifier.account_approved(user: users("user"),
                                      organization: organizations("main")).deliver_now

    assert_match "has been approved", email.body.encoded
    assert_equal [users("user").email], email.to
  end

  test "proxy expired: notify user" do
    email = Notifier.proxy_expired(users("user")).deliver_now

    assert_match "proxy certificate has expired", email.body.encoded
    assert_equal [users("user").email], email.to
  end

  test "grant will expire: notify admin users" do
    assert_emails organizations("main").users.admins.count do
      @email = Notifier.grant_expired(organization: organizations("main")).deliver_now
    end

    assert_match "you will run out of grants", @email.body.encoded
    assert_equal organizations("main").users.admins.pluck(:email), @email.to
  end
end
