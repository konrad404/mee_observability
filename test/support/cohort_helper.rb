# frozen_string_literal: true

module CohortHelper
  def add_cohort(name)
    click_on I18n.t("cohorts.index.add")
    fill_in "Name", with: name
    click_on "Add"
  end

  def navigate_to_cohorts
    visit root_path
    click_on "Cohorts"
  end

  def patients_text(page, *ids)
    id = ids.map { |id| id.respond_to?(:to_key) ? dom_id(id) : id }.join("_")
    page.find("turbo-frame[id='#{id}']", visible: false).text
  end
end
