# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
module Artifacts
  class IndexTest < ApplicationSystemTestCase
    include ActionDispatch::TestProcess::FixtureFile

    def setup
      @organization = organizations("main")
      in_organization! @organization
      login_as users("user")
      @organization.artifacts.attach(io: fixture_file_upload("outdated_proxy"), filename: "output")
      visit artifacts_path
    end

    test "it shows general artifact information" do
      assert_text @organization.artifacts.first.filename
      assert_link I18n.t("artifacts.artifact.edit")
      assert_button I18n.t("artifacts.artifact.destroy")
    end
  end
end
