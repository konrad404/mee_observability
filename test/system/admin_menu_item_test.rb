# frozen_string_literal: true

require "application_system_test_case"

class AdminMenuItemTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
  end
  test "admin can see it" do
    login_as users("admin")

    assert_text "ADMINISTRATION"
  end

  test "normal user cannot see it" do
    login_as users("user")
    visit root_path

    assert_no_text "ADMINISTRATION"
  end
end
