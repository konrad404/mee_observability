# frozen_string_literal: true

require "application_system_test_case"

class RootDomainTest < ApplicationSystemTestCase
  setup do
    in_root!
  end

  test "returns list of registered organizations" do
    visit root_path

    assert_text organizations("main").name
    assert_text organizations("other").name
  end

  test "returns to root domain, when organization is not found" do
    visit root_url(subdomain: "organization-not-found")

    assert_text "Our domain organizations"
  end

  test "allows to login" do
    login_as(users("user"))

    assert_selector :xpath, ".//form[@action='#{logout_path}']", visible: :all
  end

  test "allows to logout" do
    user = users("user")

    login_as(user)

    visit root_path
    click_on user.name
    click_on "Logout"

    assert_text "Login"
  end

  test "admin can create organization" do
    login_as(users("admin"))

    visit root_path
    assert_text "New organization"

    click_on "New organization"
    assert_current_path new_organization_path
  end

  test "regular user cannot create organization" do
    login_as(users("user"))
    visit root_path
    assert_no_text "New Organization"
  end
end
