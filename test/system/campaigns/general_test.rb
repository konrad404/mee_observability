# frozen_string_literal: true

require "application_system_test_case"

class Campaigns::GeneralTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "Organization member doesn't see campaigns from other organization" do
    other_campaign = create(:campaign, organization: organizations("other"))

    visit campaigns_path
    assert_no_text other_campaign.name

    visit other_campaign
    assert_current_path "/"
  end

  test "Organization member can remove a campaign" do
    create :campaign
    visit campaigns_path

    accept_alert do
      find("button[title='Delete campaign']").click
    end

    assert_text I18n.t("campaigns.empty.title")
  end

  test "Organization member can view the campaign page" do
    campaign = create :campaign
    visit campaigns_path
    click_on campaign.name
    assert_text "Output files of this campaign"
    assert_equal current_path, campaign_path(campaign)
  end

  test "Organization member can navigate to campaign's cohort" do
    campaign = create :campaign
    visit campaign_path(campaign)
    click_on I18n.t("cohorts.header.name", name: campaign.cohort.name)
    assert_text "Cohort patients"
    assert_equal current_path, cohort_patients_path(campaign.cohort)
  end
end
