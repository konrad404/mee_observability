# frozen_string_literal: true

require "application_system_test_case"

class Admin::GrantsTest < ApplicationSystemTestCase
  include ChoiceJSHelper
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "are limited to organization grants" do
    other_org_grant = create(:grant, organization: organizations("other"))

    visit admin_grants_path

    assert_text grants("cpu").name
    assert_text grants("gpu").name
    assert_no_text other_org_grant.name
  end

  test "admin can create new grant" do
    visit admin_grants_path

    click_link I18n.t("admin.grants.index.add")

    fill_in "Name", with: "test-grant"
    choicejs_select "cpu", from: "Grant types"
    fill_in "Starts at", with: "02/02/2010"
    fill_in "Ends at", with: "01/01/2020"

    assert_changes "Grant.count" do
      click_on I18n.t("helpers.submit.grant.create")
      assert_text "Registered grants"
    end
  end

  test "adminI can edit existing grant" do
    grant = grants("cpu")
    visit edit_admin_grant_path(grant)

    fill_in "Name", with: "updated-name"
    click_on I18n.t("helpers.submit.grant.update")

    assert_text "updated-name"
    assert_equal "updated-name", grant.reload.name
  end
end
