# frozen_string_literal: true

require "application_system_test_case"

class Admin::DataFileTypesTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "should see only organization data file types" do
    other_org_dft = create(:data_file_type, organization: organizations("other"))

    visit admin_data_file_types_path

    assert_text data_file_types("image").name
    assert_text data_file_types("segmentation").name
    assert_no_text other_org_dft.name
  end

  test "Admin can create new data file type" do
    visit admin_data_file_types_path

    click_on "New data file type"

    fill_in "Key", with: "txt"
    fill_in "Name", with: "txt file"
    fill_in "Pattern", with: ".*\.txt"
    select "text"

    assert_changes "DataFileType.count" do
      click_on "Create Data file type"
      assert_text "txt file"
    end
  end

  test "Admin can edit data file type" do
    dft = data_file_types("image")

    visit edit_admin_data_file_type_path(dft)

    fill_in "Name", with: "updated name"
    click_on "Update Data file type"

    assert_text "updated name"
    assert_no_text dft.name
  end
end
