# frozen_string_literal: true

require "application_system_test_case"

class Admin::LicensesTest < ApplicationSystemTestCase
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "should see only organization licenses" do
    other_org_license = create(:license, organization: organizations("other"))

    visit admin_licenses_path

    assert_text licenses("ansys").name
    assert_no_text other_org_license.name
  end

  test "Admin can create new license" do
    visit admin_licenses_path

    click_on I18n.t("admin.licenses.index.add")

    fill_in "Name", with: "test-license"
    fill_in "Starts at", with: "01/01/2010"
    fill_in "Ends at", with: "01/01/2020"

    click_on I18n.t("admin.licenses.entries.add")
    fill_in "Key", with: "aa_bb"
    fill_in "Value", with: "https://aa_bb.fake"

    assert_changes "License.count" do
      click_on I18n.t("helpers.submit.license.create")
      # Ensure page is fully rendered
      assert_text "Registered licenses"
    end

    license = License.last
    assert_equal 1, license.config.size
    assert_equal "aa_bb", license.config.first.key
    assert_equal "https://aa_bb.fake", license.config.first.value
  end
end
