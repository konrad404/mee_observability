# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"

module OrganizationPipelines
  class IndexTest < ApplicationSystemTestCase
    def setup
      in_organization! organizations("main")
      login_as users("user")
      @organization = organizations("main")
    end

    test "shows pipelines list" do
      @pipeline = create(:pipeline, runnable: @organization, name: "p1")
      visit pipelines_path

      assert_link "p1", href: pipeline_path(@pipeline)
      assert_text @pipeline.flow.name
      assert_text @pipeline.user.name
    end

    test "doesn't show compare button when one pipeline exists" do
      @pipeline = create(:pipeline, runnable: @organization, name: "p1")
      visit pipelines_path

      assert_no_selector "input[value='#{I18n.t('runnables.pipelines.tab_compare.compare')}']"
    end

    test "shows compare button when more than one pipeline exists" do
      @pipeline = create(:pipeline, runnable: @organization, name: "p1")
      create(:pipeline, runnable: @organization)
      visit pipelines_path

      assert_selector "input[value='#{I18n.t('runnables.pipelines.tab_compare.compare')}']"
    end

    test "edit button for owner" do
      @pipeline = create(:pipeline, runnable: @organization, name: "p1")
      visit pipelines_path

      assert_link I18n.t("pipelines.show.edit")
      click_link I18n.t("pipelines.show.edit")

      assert_current_path edit_pipeline_path(@pipeline)
    end

    test "doesn't show edit button for non-owner" do
      create(:pipeline, runnable: @organization, user: users("admin"))

      visit pipelines_path

      assert_no_link I18n.t("pipelines.show.edit")
    end

    test "remove button as owner" do
      pipeline = create(:pipeline, runnable: @organization, name: "p1")
      visit pipelines_path

      assert_difference "Pipeline.count", -1 do
        accept_alert do
          click_button I18n.t("pipelines.show.delete")
        end
        assert_text I18n.t("pipelines.destroy.success", name: pipeline.name)
      end
    end

    test "doesn't show remove button for non-owner" do
      create(:pipeline, runnable: @organization, user: users("admin"))

      visit pipelines_path

      assert_no_link I18n.t("pipelines.show.remove")
    end

    test "add pipeline button" do
      visit pipelines_path

      assert_link I18n.t("pipelines.index.add")
      click_link I18n.t("pipelines.index.add")

      assert_current_path new_pipeline_path
    end

    test "lets navigate to a given pipeline" do
      @pipeline = create(:pipeline, runnable: @organization, name: "p1")
      visit pipelines_path
      click_link @pipeline.name

      assert_current_path pipeline_path(@pipeline)
    end
  end
end
