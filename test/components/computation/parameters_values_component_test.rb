# frozen_string_literal: true

require "test_helper"

class Computation::ParametersValuesComponentTest < ViewComponent::TestCase
  test "show empty message when parameters values not present" do
    computation = build(:computation)

    render_inline(Computation::ParametersValuesComponent.new(computation:))

    assert_text "No parameters values saved yet"
  end

  test "shows parameters values" do
    computation = build(:computation)
    computation.parameter_values = [
      ParameterValue::ModelVersion.new(name: "Model version", version: "master"),
      ParameterValue::ModelVersion.new(name: "Other parameter value", version: "some value")
    ]

    render_inline(Computation::ParametersValuesComponent.new(computation:))

    assert_text "Model version"
    assert_text "master"
    assert_text "Other parameter value"
    assert_text "some value"
  end
end
