# frozen_string_literal: true

require "test_helper"

class Computation::StatusComponentTest < ViewComponent::TestCase
  setup do
    @pipeline = create(:pipeline)
    @computation = create(:computation, pipeline: @pipeline)
  end

  test "notify user about discarded flow" do
    @pipeline.flow.discard!

    render_inline(Computation::StatusComponent.new(computation: @computation))

    assert_text "!"
    assert_selector "i[title='#{I18n.t('steps.readonly', step: @computation.pipeline_step)}']"
    assert_selector "span[class='fa-layers-counter status-with-warning']"
  end
end
