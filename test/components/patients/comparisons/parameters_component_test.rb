# frozen_string_literal: true

require "test_helper"

class Patients::Comparisons::ParametersComponentTest < ViewComponent::TestCase
  test "compare parameter versions" do
    m1 = ParameterValue::ModelVersion.new(key: "mv", name: "Model version", version: "master")
    m2 = ParameterValue::ModelVersion.new(key: "mv", name: "Model version", version: "branch")

    render_inline(Patients::Comparisons::ParametersComponent.new(first: [m1], second: [m2]))

    assert_text "Model version"
    assert_text "master"
    assert_text "branch"
  end

  test "show undefined when one parameter is not found" do
    m1 = ParameterValue::ModelVersion.new(key: "mv", name: "Model version", version: "master")

    render_inline(Patients::Comparisons::ParametersComponent.new(first: [m1], second: []))

    assert_text "Model version"
    assert_text "master"
    assert_text "[undefined]"
  end
end
