# frozen_string_literal: true

require "test_helper"

class GuardedProxyExecutorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "invoke block when proxy is valid" do
    invoked = false
    GuardedProxyExecutor.new(users("user")).call do
      invoked = true
    end

    assert invoked, "Block should be invoked"
  end

  test "don't notify when proxy is valid" do
    assert_no_changes "ActionMailer::Base.deliveries.count" do
      GuardedProxyExecutor.new(users("user")).call
    end
  end

  test "don't invoke block when proxy is not valid" do
    user = users("user")
    user.update(proxy: nil)

    GuardedProxyExecutor.new(user).call do
      flunk "Block should not be invoked"
    end
  end

  test "Notify user via email about invalid/outdated proxy" do
    user = users("user")
    user.update(proxy: nil)

    perform_enqueued_jobs do
      assert_changes "ActionMailer::Base.deliveries.count" do
        GuardedProxyExecutor.new(user).call
      end
    end
  end

  test "sends 1 email every day about proxy expiration" do
    user = users("user")
    user.update(proxy: nil)

    assert_changes "ActionMailer::Base.deliveries.count", 2 do
      perform_enqueued_jobs do
        GuardedProxyExecutor.new(user).call
        GuardedProxyExecutor.new(user).call

        travel 25.hours
        GuardedProxyExecutor.new(user).call
      end
    end
  end
end
