# frozen_string_literal: true

require "test_helper"

class Pipelines::RemoveDuplicateOutputsTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile

  setup do
    @pipeline = create(:pipeline, mode: :automatic, user: users("user"))
    @pipeline.outputs
    .attach(io: fixture_file_upload("outdated_proxy"),
            filename: "duplicate_name")
    @pipeline.outputs
    .attach(io: fixture_file_upload("outdated_proxy"),
            filename: "duplicate_name")
    @pipeline.outputs
    .attach(io: fixture_file_upload("outdated_proxy"),
            filename: "unique_name")
  end

  test "remove duplicate outputs" do
    Pipelines::RemoveDuplicateOutputs.new(@pipeline).call
    assert_equal 2, @pipeline.reload.outputs.size
  end
end
