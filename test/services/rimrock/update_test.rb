# frozen_string_literal: true

require "test_helper"

class Rimrock::UpdateTest < ActiveSupport::TestCase
  include RimrockHelper
  include ActiveJob::TestHelper
  setup do
    @user = users("user")
  end

  test "do nothing when user does not have active jobs" do
    create(:computation, status: "finished", user: @user)

    # if it makes a request, it would throw an error of not having stubbed request
    Rimrock::Update.new(@user).call
  end

  test "asks about jobs when user has active computations" do
    create(:computation, status: "finished", user: @user)
    c1 = create(:computation, status: "queued", job_id: "job1", user: @user)
    c2 = create(:computation, status: "queued", job_id: "job2", user: @user)

    stub_computation_logs_request(c1, c2)

    stub_request(:get, "https://rimrock.plgrid.pl/api/jobs?format=short&job_id=job1&job_id=job2")
      .to_return(status: 200, headers: {},
                 body: '[{"job_id": "job1", "status": "FINISHED"},
                  {"job_id": "job2", "status": "RUNNING"}]')

    Rimrock::Update.new(@user).call
    c1.reload
    c2.reload

    assert_equal "finished", c1.status
    assert_equal "running", c2.status
  end

  test "logs when error updating computations" do
    create(:computation, status: "queued", job_id: "job1", user: @user)

    stub_request(:get, "https://rimrock.plgrid.pl/api/jobs?format=short&job_id=job1")
      .to_return(status: 500, body: "error details", headers: {})

    Rails.logger.expects(:warn).
      with(I18n.t("rimrock.update.internal",
                  user: @user.name, details: "error details"))

    Rimrock::Update.new(@user).call
  end

  test "triggers callback after computation is finished and queues delete duplicate job" do
    c1 = create(:computation, status: "queued", job_id: "job1", user: @user)

    stub_computation_logs_request(c1)

    stub_request(:get, "https://rimrock.plgrid.pl/api/jobs?format=short&job_id=job1")
      .to_return(status: 200, headers: {},
                 body: '[{"job_id": "job1", "status": "FINISHED"}]')

    callback_instance = mock
    callback_instance.expects(:call)
    callback = stub(new: callback_instance)

    assert_enqueued_jobs(1, only: Pipelines::RemoveDuplicateOutputsJob) do
      Rimrock::Update.new(@user, on_finish_callback: callback).call
    end
  end

  test "triggers update when status changed" do
    create(:computation, status: "queued", job_id: "job1", user: @user)
    create(:computation, status: "running", job_id: "job2", user: @user)

    stub_request(:get, "https://rimrock.plgrid.pl/api/jobs?format=short&job_id=job1&job_id=job2")
      .to_return(status: 200, headers: {},
                 body: '[{"job_id": "job1", "status": "RUNNING"},
                  {"job_id": "job2", "status": "RUNNING"}]')

    callback_instance = mock
    callback_instance.expects(:call)
    updater = stub(new: callback_instance)

    Rimrock::Update.new(@user, updater:).call
  end
end
