# frozen_string_literal: true

require "test_helper"

class Computations::OutputsComputationTest < ActionDispatch::IntegrationTest
  def setup
    in_organization! organizations("main")
    @computation = create(:computation, secret: SecureRandom.base58, status: :running)
    @pipeline = @computation.pipeline
  end

  test "create returns 400 if no file is provided" do
    post computation_outputs_path(@computation, secret: @computation.secret)
    assert_response :bad_request
  end

  test "create returns 201 and creates file" do
    assert_difference "@pipeline.outputs.count" do
      post computation_outputs_path(@computation, secret: @computation.secret), params: { file: fixture_file_upload("file.zip") }
    end
    assert_response :created
    assert_equal data_file_types("image").data_type,
      @pipeline.outputs_blobs.first.metadata[:file_type]
  end

  test "create returns 500 for upload failure" do
    ActiveStorage::Attachment.stubs(:create).returns(false)

    assert_no_difference "@pipeline.outputs.count" do
      post computation_outputs_path(@computation, secret: @computation.secret), params: { file: fixture_file_upload("file.zip") }
    end
    assert_response :internal_server_error
  end
end
