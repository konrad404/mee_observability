# frozen_string_literal: true

require "test_helper"

class Computations::ArtifactsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @organization = organizations("main")
    in_organization! @organization
    @computation = create(:computation, secret: SecureRandom.base58, status: :running)
  end

  test "show returns 404 if file is not found" do
    get computation_artifacts_path(@computation, secret: @computation.secret, name: "does not exist")
    assert_response :not_found
  end

  test "show serves file from s3" do
    @organization.artifacts.attach(io: fixture_file_upload("outdated_proxy"),
                                      filename: "artifact.txt",
                                      metadata: { file_type: @file_type })

    get computation_artifacts_path(@computation, secret: @computation.secret, name: "artifact", format: "txt")
    assert_redirected_to @organization.artifacts_blobs.first
  end
end
