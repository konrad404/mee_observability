# frozen_string_literal: true

require "test_helper"

class Computations::InputsComputationTest < ActionDispatch::IntegrationTest
  def setup
    in_organization! organizations("main")
    @computation = create(:computation, secret: SecureRandom.base58, status: :running)
    @pipeline = @computation.pipeline
    @file_type = "type"
  end

  test "show returns 404 if file is not found" do
    get computation_inputs_path(@computation, secret: @computation.secret, type: "other_type")
    assert_response :not_found
  end

  test "show serves file from s3" do
    @pipeline.inputs.attach(io: fixture_file_upload("outdated_proxy"),
                                          filename: "input",
                                          metadata: { file_type: @file_type })

    get computation_inputs_path(@computation, secret: @computation.secret, type: @file_type)
    assert_redirected_to @pipeline.inputs_blobs.first
  end
end
