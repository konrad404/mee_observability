# frozen_string_literal: true

ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
require "webmock/minitest"
require "fixture_factory"
require "mocha/minitest"
require_relative "factories"

Dir[Rails.root.join("test/support/**/*.rb")].each { |f| require f }

ActiveRecord::FixtureSet.context_class.include CertHelper

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include OauthHelper
  GitlabHelper.default_gitlab_stub

  def login_as(user, teams: "#{organizations("main").plgrid_team_id}(#{organizations("main").name})")
    stub_plgrid_oauth(user, teams)

    get "#{root_path}auth/open_id"
    follow_redirect!
  end

  def in_organization!(organization)
    self.default_url_options =
      { script_name: "/#{OrganizationSlug.encode(organization.id)}" }
  end

  def in_root!
    self.default_url_options = {}
  end
end

OmniAuth.config.test_mode = true

WebMock.disable_net_connect!(allow_localhost: true,
                             allow: "chromedriver.storage.googleapis.com")
