# frozen_string_literal: true

require "test_helper"

class ParameterValue::NumberTest < ActiveSupport::TestCase
  setup do
    @pv = build(:number_parameter_value)
  end

  test "validates min" do
    @pv.parameter.min = 1

    @pv.value = 0
    assert_not @pv.valid?

    @pv.value = 2
    assert @pv.valid?
  end

  test "validates max" do
    @pv.parameter.max = 1

    @pv.value = 2
    assert_not @pv.valid?

    @pv.value = 0
    assert @pv.valid?
  end

  test "validates it is number" do
    @pv.value = nil
    assert_not @pv.valid?

    @pv.value = "not a number"
    assert_not @pv.valid?

    @pv.value = 4
    assert @pv.valid?
  end

  test "has default value" do
    @pv.parameter.default_value = 2

    assert_equal 2, @pv.value

    @pv.value = ""
    assert_equal "", @pv.value

    @pv.value = 3
    assert_equal 3, @pv.value
  end
end
