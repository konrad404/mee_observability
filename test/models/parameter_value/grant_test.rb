# frozen_string_literal: true

require "test_helper"

class ParameterValue::GrantTest < ActiveSupport::TestCase
  setup do
    @pv = build(:grant_parameter_value)
  end

  test "allows only active grants" do
    @pv.value = "nonexisting"
    assert_not @pv.valid?
    assert_match "not included in the list", @pv.errors[:value].first

    @pv.value = grants("cpu").name
    assert @pv.valid?
  end

  test "allows only grants of correct type" do
    # step that is created in fixtures allows only cpu grants
    @pv.value = grants("gpu").name
    assert_not @pv.valid?
  end

  test "default grant name is returned when value is not set and there is only one grant" do
    assert_equal grants("cpu").name, @pv.default_value
  end

  test "returns value when it is set" do
    @pv.value = "othervalue"
    assert_equal "othervalue", @pv.default_value
  end

  test "returns nothing when value is not set and there is more than one grant" do
    create(:grant)

    assert_nil @pv.default_value
  end
end
