# frozen_string_literal: true

require "test_helper"

class DataFileTypeTest < ActiveSupport::TestCase
  test "Should validate pattern" do
    assert_not build(:data_file_type, pattern: "?").valid?
    assert_not build(:data_file_type, pattern: "").valid?
    assert_not build(:data_file_type, pattern: nil).valid?

    assert build(:data_file_type, pattern:  /^dft.txt$/).valid?
  end

  test "Should match according to pattern" do
    dft = build(:data_file_type, pattern: /^dft.txt$/)

    assert_match dft, "dft.txt"
    assert_no_match dft, "abc.1"
  end
end
