# frozen_string_literal: true

require "test_helper"

class ComputationTest < ActiveSupport::TestCase
  test "#active returns only new, queued or running computations" do
    computation = create(:computation, status: "new")
    assert_equal [computation], Computation.active

    computation.update(status: "queued")
    assert_equal [computation], Computation.active

    computation.update(status: "running")
    assert_equal [computation], Computation.active

    computation.update(status: "finished")
    assert_equal [], Computation.active
  end

  test "#submitted returns only queued and running computations" do
    create(:computation, status: "new")
    queued = create(:computation, status: "queued")
    running = create(:computation, status: "running")

    assert_equal [queued, running].sort, Computation.submitted.sort
  end

  test "Computation is configured only if tag or branch is set" do
    assert_not build(:computation).configured?
    assert build(:computation, parameter_values_attributes: { tag_or_branch: { value: "master" } }).configured?
  end
end
