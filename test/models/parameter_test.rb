# frozen_string_literal: true

require "test_helper"

class Parameter::Foo < Parameter
end

class ParameterValue::Foo
end

class ParameterTest < ActiveSupport::TestCase
  test "calculate value class basing on class name" do
    assert_equal ParameterValue::Foo, Parameter::Foo.new.value_class
  end
end
