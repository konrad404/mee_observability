# frozen_string_literal: true

require "test_helper"

class PipelineTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  include ActionDispatch::TestProcess::FixtureFile

  test "generates relative pipeline id" do
    patient = create(:patient)

    p1 = create(:pipeline, runnable: patient)
    p2 = create(:pipeline, runnable: patient)

    assert_equal 1, p1.iid
    assert_equal 2, p2.iid
  end

  test "returns data file scoped into pipeline" do
    patient = create(:patient)
    p1, p2 = create_list(:pipeline, 2, runnable: patient)

    image = data_file_types("image")
    off_mesh = create(:data_file_type)
    graphics = create(:data_file_type)
    estimated_parameters = create(:data_file_type)

    p1.outputs.attach(io: fixture_file_upload("outdated_proxy"),
                              filename: "p1 image output",
                              metadata: { file_type: image.data_type })

    p2.outputs.attach(io: fixture_file_upload("outdated_proxy"),
                              filename: "p2 image output",
                              metadata: { file_type: image.data_type })
    p1.inputs.attach(io: fixture_file_upload("outdated_proxy"),
           filename: "p1 off mesh output",
           metadata: { file_type: off_mesh.data_type })
    p2.inputs.attach(io: fixture_file_upload("outdated_proxy"),
           filename: "p2 graphics output",
           metadata: { file_type: graphics.data_type })
    patient.inputs.attach(io: fixture_file_upload("outdated_proxy"),
                            filename: "input",
                            metadata: { file_type: estimated_parameters.data_type })

    assert_equal "p1 image output", p1.data_file(image.data_type).filename.to_s
    assert_equal "p2 image output", p2.data_file(image.data_type).filename.to_s
    assert_nil p2.data_file(off_mesh.data_type)
    assert_equal "p2 graphics output", p2.data_file(graphics.data_type).filename.to_s
    assert_equal "input", p1.data_file(estimated_parameters.data_type).filename.to_s
    assert_equal "input", p2.data_file(estimated_parameters.data_type).filename.to_s
  end

  test "calls StatusCalculator when asked for status" do
    Pipeline::StatusCalculator.any_instance.expects(:calculate)
    pipeline = create(:pipeline)
    pipeline.update_status!
  end

  test "returns creator name" do
    user = users("user")
    pipeline = create(:pipeline, user:)

    assert_equal user.name, pipeline.owner_name
  end

  test "returns information about deleted user when owner is nil" do
    user = users("user")
    pipeline = create(:pipeline, user:)

    user.destroy!
    pipeline.reload

    assert_equal "(deleted user)", pipeline.owner_name
  end

  test "destroys discarded flow when it has only one pipelines" do
    flow = create(:flow, discarded_at: Time.current)
    pipeline = create(:pipeline, flow:)

    assert_changes "Flow.count", -1 do
      pipeline.destroy
    end
  end

  test "doesn't destroy the flow when it has more pipelines" do
    flow = create(:flow, discarded_at: Time.current)
    pipeline = create(:pipeline, flow:)
    create(:pipeline, flow:)

    assert_no_changes "Flow.count" do
      pipeline.destroy
    end
  end

  test "attaching a file for pipeline queues Pipelines::StartRunnableJob" do
    Current.organization = organizations("main")
    pipeline = create(:pipeline)
    assert_enqueued_with(job: Pipelines::StartRunnableJob, args: [pipeline]) do
      pipeline.add_input(fixture_file_upload("outdated_proxy"))
    end
  end

  test "picks output file when output file is present" do
    pipeline = create(:pipeline)
    pipeline.outputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "output", metadata: { file_type: "type" })

    assert_equal pipeline.pick_file("type"), pipeline.outputs_blobs.first
  end

  test "picks output file when input and output file is present" do
    pipeline = create(:pipeline)
    pipeline.outputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "output", metadata: { file_type: "type" })
    pipeline.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input", metadata: { file_type: "type" })

    assert_equal pipeline.pick_file("type"), pipeline.outputs_blobs.first
  end

  test "picks input file when only input file is present" do
    pipeline = create(:pipeline)
    pipeline.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input", metadata: { file_type: "type" })

    assert_equal pipeline.pick_file("type"), pipeline.inputs_blobs.first
  end
end
