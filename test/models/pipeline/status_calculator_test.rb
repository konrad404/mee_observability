# frozen_string_literal: true

require "test_helper"

class Pipeline::StatusCalculatorTest < ActiveSupport::TestCase
  test "Should be success when all steps are completed with success" do
    pipeline = create(:pipeline, computations: [
      create(:computation, status: :finished),
      create(:computation, status: :finished)
    ])
    assert_equal :success, Pipeline::StatusCalculator.new(pipeline).calculate
  end

  test "Should be error when any step finished with error" do
    pipeline = create(:pipeline, computations: [
      create(:computation, status: :finished),
      create(:computation, status: :error)
    ])

    assert_equal :error, Pipeline::StatusCalculator.new(pipeline).calculate
  end

  test "Should be running when any step is running" do
    pipeline = create(:pipeline, computations: [
      create(:computation, status: :finished),
      create(:computation, status: :running)
    ])

    assert_equal :running, Pipeline::StatusCalculator.new(pipeline).calculate

    pipeline.computations.last.update(status: :new)
    assert_equal :running, Pipeline::StatusCalculator.new(pipeline).calculate

    pipeline.computations.last.update(status: :queued)
    assert_equal :running, Pipeline::StatusCalculator.new(pipeline).calculate
  end

  test "Should be waiting when is not running and any step is waiting for input" do
    pipeline = create(:pipeline, computations: [
      build(:computation, status: :created, pipeline_step: "rom")
    ])

    assert_equal :waiting, Pipeline::StatusCalculator.new(pipeline).calculate
  end
end
