# frozen_string_literal: true

require "test_helper"

class PatientTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  include ActionDispatch::TestProcess::FixtureFile

  ["", "new"].each do |case_number|
    test "'#{case_number}' should not be a valid case number" do
      assert_not build(:patient, case_number:).valid?
    end
  end

  test "#status returns last pipeline status" do
    patient = create(:patient)
    create(:pipeline, runnable: patient, status: :error)
    create(:pipeline, runnable:  patient, status: :success)

    assert_equal "success", patient.status
  end

  test "attaching a file for patient queues Pipelines::StartRunnableJob" do
     Current.organization = organizations("main")
     patient = create(:patient)
     create(:pipeline, runnable: patient)
     create(:pipeline, runnable: patient)
     assert_enqueued_jobs(2, only: Pipelines::StartRunnableJob,) do
        patient.add_input(fixture_file_upload("outdated_proxy"))
      end
   end
end
