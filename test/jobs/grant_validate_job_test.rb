# frozen_string_literal: true

require "test_helper"

class GrantValidateJobTest < ActiveJob::TestCase
  include ActionMailer::TestHelper

  def setup
    @org = organizations("other")
  end

  test "do nothing when grant ends not soon" do
    create(:grant, ends_at: Time.now + 1.year, organization: @org)

    assert_no_emails do
      Organization::ValidateGrantJob.perform_now(@org)
    end
  end

  test "notify when grant ends soon and there is no continuation" do
    create(:grant, ends_at: Time.now + 1.day, organization: @org)

    assert_enqueued_emails 1 do
      Organization::ValidateGrantJob.perform_now(@org)
    end
  end

  test "do nothing when grant ends soon, but there is a continuation" do
    create(:grant, starts_at: Time.now - 1.day, ends_at: Time.now + 1.day, organization: @org)
    create(:grant, starts_at: Time.now + 1.day, ends_at: Time.now + 1.year, organization: @org)

    assert_no_emails do
      Organization::ValidateGrantJob.perform_now(@org)
    end
  end
end
