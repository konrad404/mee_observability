# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class Rimrock::StartJobTest < ActiveJob::TestCase
  test "triggers user computations update" do
    computation = create(:computation)

    callable = MiniTest::Mock.new
    callable.expect(:call, true)
    callable.expect(:call, true)

    start = MiniTest::Mock.new
    start.expect(:call, callable, [computation])

    update = MiniTest::Mock.new
    update.expect(:call, callable, [computation])

    Rimrock::Start.stub(:new, start) do
      ComputationUpdater.stub(:new, update) do
        Rimrock::StartJob.perform_now(computation)
      end
    end

    callable.verify
  end

  test "triggers computation update after error" do
    computation = create(:computation)

    callable = MiniTest::Mock.new
    callable.expect(:call, true)

    throwable = MiniTest::Mock.new
    throwable.expect(:call, true)

    start = Object.new
    def start.call = raise

    update = MiniTest::Mock.new
    update.expect(:call, callable, [computation])

    Rimrock::Start.stub(:new, start) do
      ComputationUpdater.stub(:new, update) do
        Rimrock::StartJob.perform_now(computation)
      end
    end

    callable.verify
  end
end
