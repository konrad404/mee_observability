# frozen_string_literal: true

require "test_helper"

class FlowsControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "I can remove flow" do
    assert_difference "Flow.count", -1 do
      delete flow_path(flows("first"))
    end

    assert_redirected_to flows_path
  end

  test "I cannot remove step from other organization" do
    other_flow = create(:flow, organization: organizations("other"))

    assert_no_difference "DataFileType.count" do
      delete flow_path(other_flow)
    end
  end
end
