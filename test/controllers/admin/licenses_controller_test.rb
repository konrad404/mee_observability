# frozen_string_literal: true

require "test_helper"

class Admin::LicensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "should destroy license from owned organization" do
    assert_difference "License.count", -1 do
      delete admin_license_path(licenses("ansys"))
    end

    assert_redirected_to admin_licenses_url
  end

  test "cannot remove linces from other organization" do
    other_license = create(:license, organization: organizations("other"))

    assert_no_difference "License.count" do
      delete admin_license_url(other_license)
    end

    assert_redirected_to root_url
  end
end
