# frozen_string_literal: true

require "test_helper"

class PatientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
  end

  test "Patients are not accessible for anonymous user" do
    get patients_path
    assert_redirected_to root_path
  end

  test "Organization member can see all patients" do
    p1, p2 = create_list(:patient, 2)
    other_org_patient = create(:patient, organization: organizations("other"))

    login_as users("user")
    get patients_path

    assert_response :success
    assert_match p1.case_number, @response.body
    assert_match p2.case_number, @response.body
    assert_no_match other_org_patient.case_number, @response.body
  end

  test "Organization member can see organization patient" do
    patient = create(:patient)
    other_org_patient = create(:patient, organization: organizations("other"))

    login_as users("user")

    get patient_path(patient)
    assert_response :success
    assert_match patient.case_number, @response.body


    get patient_path(other_org_patient)
    assert_redirected_to root_path, "User should not be able to see other org patient"
  end

  test "Organization member remove patient" do
    patient = create(:patient)

    login_as users("user")

    assert_difference "Patient.count", -1 do
      delete patient_path(patient)
    end

    assert_redirected_to patients_path
  end
end
