# frozen_string_literal: true

require "test_helper"

class TimeScopesHelperTest < ActionView::TestCase
  test "#time_scope_button_class" do
    assert_equal "badge-secondary", time_scope_button_class(build(:expired_grant))
    assert_equal "badge-success", time_scope_button_class(build(:grant))
    assert_equal "badge-info", time_scope_button_class(build(:future_grant))
  end

  test "#time_scope_button_text" do
    assert_equal "outdated", time_scope_button_text(build(:expired_grant))
    assert_equal "active", time_scope_button_text(build(:grant))
    assert_equal "future", time_scope_button_text(build(:future_grant))
  end
end
