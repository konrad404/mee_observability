# frozen_string_literal: true

if Rails.env.development? && plgrid_login = ENV["ADMIN_PLGRID_LOGIN"].presence
  User.create!(plgrid_login:,
              email: "#{plgrid_login}@mee.test",
              first_name: plgrid_login, last_name: plgrid_login,
              roles: [:admin], terms: true)

end

GrantType.find_or_create_by!(name: "cpu")
GrantType.find_or_create_by!(name: "gpu")
Site.find_or_create_by!(name: "prometheus",
                        host: "prometheus.cyfronet.pl", host_key: "prometheus")
Site.find_or_create_by!(name: "ares",
                        host: "ares.cyfronet.pl", host_key: "ares")
Site.find_or_create_by!(name: "athena",
                        host: "athena.cyfronet.pl", host_key: "athena")
