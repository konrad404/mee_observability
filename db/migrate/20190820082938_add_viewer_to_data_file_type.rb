# frozen_string_literal: true

class AddViewerToDataFileType < ActiveRecord::Migration[5.2]
  def change
    add_column :data_file_types, :viewer, :string
  end
end
