# frozen_string_literal: true

class CreateArtifact < ActiveRecord::Migration[7.0]
  def change
    create_table :artifacts do |t|
      t.string :name, null: false
      t.belongs_to :organization, null: false, foreign_key: true

      t.index :name, unique: true
      t.timestamps null: false
    end
  end
end
