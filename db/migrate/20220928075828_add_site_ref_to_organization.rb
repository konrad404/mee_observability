# frozen_string_literal: true

class AddSiteRefToOrganization < ActiveRecord::Migration[7.0]
  def change
    add_reference :organizations, :site, foreign_key: true
  end
end
