# frozen_string_literal: true

class AddConfigToOrganization < ActiveRecord::Migration[5.2]
  def change
    add_column :organizations, :config, :jsonb, default: {}, null: false
  end
end
