# frozen_string_literal: true

class CreatePrerequisites < ActiveRecord::Migration[5.2]
  def change
    create_table :prerequisites do |t|
      t.belongs_to :step, null: false, foreign_key: true
      t.belongs_to :data_file_type, null: false, foreign_key: true

      t.timestamps
    end

    add_index :prerequisites, [:step_id, :data_file_type_id], unique: true
  end
end
