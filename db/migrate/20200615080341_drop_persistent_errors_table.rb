# frozen_string_literal: true

class DropPersistentErrorsTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :persistent_errors
  end
end
