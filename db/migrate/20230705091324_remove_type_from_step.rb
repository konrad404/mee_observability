# frozen_string_literal: true

class RemoveTypeFromStep < ActiveRecord::Migration[7.0]
  def change
    remove_column :steps, :type, :string, null: false
  end
end
