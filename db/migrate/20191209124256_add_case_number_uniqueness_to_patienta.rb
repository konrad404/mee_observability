# frozen_string_literal: true

class AddCaseNumberUniquenessToPatienta < ActiveRecord::Migration[5.2]
  def change
    add_index :patients, [:case_number, :organization_id], unique: true
  end
end
