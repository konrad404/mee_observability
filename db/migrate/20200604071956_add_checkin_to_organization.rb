# frozen_string_literal: true

class AddCheckinToOrganization < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :checkin_enabled, :boolean, default: false, null: false
  end
end
