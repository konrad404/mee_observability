# frozen_string_literal: true

class CreateFlowSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :flow_steps do |t|
      t.belongs_to :flow, null: false, foreign_key: true
      t.belongs_to :step, null: false, foreign_key: true
      t.integer :position, null: false, index: true

      t.timestamps
    end

    add_index :flow_steps, [:flow_id, :step_id], unique: true
  end
end
