# frozen_string_literal: true

class AddStatusToPipeline < ActiveRecord::Migration[7.0]
  def change
    add_column :pipelines, :status, :integer, default: 0
  end
end
