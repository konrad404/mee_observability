# frozen_string_literal: true

class ChangeDataFilePatientToPolymorphicFileable < ActiveRecord::Migration[6.1]
  def up
    change_table :data_files do |t|
      t.rename :patient_id, :fileable_id
      t.column :fileable_type, :string

      ActiveRecord::Base.connection.execute(
        "UPDATE data_files SET fileable_type = \'Patient\'"
      )
      t.remove_index :fileable_id
    end

    change_column_null :data_files, :fileable_type, false
    add_index :data_files, [:fileable_id, :fileable_type]
  end

  def down
    change_table :data_files do |t|
      t.rename :fileable_id, :patient_id

      ActiveRecord::Base.connection.execute(
        "DELETE FROM data_files WHERE fileable_type = \'Organization\'"
      )

      t.remove :fileable_type
      t.index :patient_id
    end
  end
end
