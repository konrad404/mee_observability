# frozen_string_literal: true

class ChangePipelinePatientToPolymorphicRunnable < ActiveRecord::Migration[6.1]
  def up
    change_table :pipelines do |t|
      t.rename :patient_id, :runnable_id
      t.column :runnable_type, :string

      ActiveRecord::Base.connection.execute(
        "UPDATE pipelines SET runnable_type = \'Patient\'"
      )

      t.remove_index :runnable_id
      t.remove_index [:runnable_id, :iid]
    end

    change_column_null :pipelines, :runnable_type, false
    add_index :pipelines, [:runnable_id, :runnable_type, :iid], unique: true
    add_index :pipelines, [:runnable_id, :runnable_type]
  end

  def down
    change_table :pipelines do |t|
      t.rename :runnable_id, :patient_id

      ActiveRecord::Base.connection.execute(
        "DELETE FROM pipelines WHERE runnable_type = \'Organization\'"
      )

      t.index :patient_id
      t.remove :runnable_type
    end
  end
end
