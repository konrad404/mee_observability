# frozen_string_literal: true

class RemoveOrganizationStorage < ActiveRecord::Migration[7.0]
  def change
    drop_table :data_files

    remove_column :organizations, :config, :jsonb, default: {}, null: false
    remove_column :organizations, :storage_config, :jsonb, default: {}, null: false
  end
end
