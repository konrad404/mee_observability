# frozen_string_literal: true

class FixPersistentErrorCounts < ActiveRecord::Migration[6.0]
  def change
    change_column_null :organizations, :persistent_errors_count, false, default: 0
    change_column_default :organizations, :persistent_errors_count, from: nil, to: 0
    change_column_null :steps, :persistent_errors_count, false, default: 0
    change_column_default :steps, :persistent_errors_count, from: nil, to: 0
  end
end
