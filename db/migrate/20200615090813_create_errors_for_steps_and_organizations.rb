# frozen_string_literal: true

class CreateErrorsForStepsAndOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :step_errors do |t|
      t.string :key, index: true, null: false
      t.string :message, null: false
      t.references :step, foreign_key: true, index: true, null: false
    end
    create_table :organization_errors do |t|
      t.string :key, index: true, null: false
      t.string :message, null: false
      t.references :organization, foreign_key: true, index: true, null: false
    end
  end
end
