# frozen_string_literal: true

class RemoveTypeFromComputation < ActiveRecord::Migration[7.0]
  def change
    remove_column :computations, :type, :string, null: false
  end
end
