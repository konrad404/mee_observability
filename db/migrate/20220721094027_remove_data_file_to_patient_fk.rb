# frozen_string_literal: true

class RemoveDataFileToPatientFk < ActiveRecord::Migration[7.0]
  def change
    remove_foreign_key :data_files, column: :fileable_id
  end
end
