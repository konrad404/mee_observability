# frozen_string_literal: true

class CreateParameterValues < ActiveRecord::Migration[6.0]
  def change
    create_table :parameter_values do |t|
      t.string :slug, null: false
      t.string :label, null: false
      t.jsonb :value_store, null: false, default: {}

      t.string :type

      t.belongs_to :computation
      t.belongs_to :parameter

      t.timestamps
    end
  end
end
