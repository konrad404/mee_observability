# frozen_string_literal: true

class AddSecretToComputation < ActiveRecord::Migration[7.0]
  def change
    add_column :computations, :secret, :string
  end
end
