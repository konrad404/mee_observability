; This file is an example config file for production nginx which is external to the docker-compose cluster.
; It handles SSL termination and acts as a proxy to the web server running as part of the cluster
; This file should be copied to the conf.d directory in nginx config directory.
; Web server port, hostname and SSL paths must be set manually.

upstream mee {
  server localhost:4000;
}

server {
  listen        80;
  server_name   mee.domain *.mee.domain
  server_tokens off;
  return        301 https://$host$request_uri;
}

server {
  listen      443 ssl;
  server_name mee.domain *.mee.domain

  ssl                 on;
  ssl_certificate     /path/to/certificate.crt;
  ssl_certificate_key /path/to/certificate_key.key;
  ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;

  access_log /var/log/nginx/mee_access.log;
  error_log  /var/log/nginx/mee_error.log;

  location / {
    proxy_redirect off;

    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $http_host;
    proxy_set_header X-Forwarded-Ssl on;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $remote_addr;

    proxy_pass http://mee;
  }
}
